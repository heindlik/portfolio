# heindlik's Portfolio changelog

## 1.0.3 (2024-01-07)

- Fixed alerts position

## 1.0.1 (2024-01-02)

- Added meta info
- Fixed Safari font bug
- Fixed mobile video player controls

## 1.0.0 (2023-12-31)

- Initial release
