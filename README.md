# Portfolio

Showcase of my work at [heindlik.cz](https://heindlik.cz)

## Built with React

- `npx create-react-app portfolio --template typescript`,

- ESLint by default (rules in [.eslintrc.json](.eslintrc.json)), for accessibility added [jsx-a11y](https://github.com/jsx-eslint/eslint-plugin-jsx-a11y#readme) `npm i -g eslint-plugin-jsx-a11y --save-dev`

- [Sass](https://www.npmjs.com/package/sass) `npm i --save-dev sass`

- [i18next](https://react.i18next.com/) for translations `npm i --save i18next react-i18next i18next-http-backend`

- [Axios](https://www.npmjs.com/package/axios) for requests `npm i --save axios`

- [React Router](https://reactrouter.com/en/main/) for routing `npm i --save-dev react-router-dom`

- [ReactSlider](https://zillow.github.io/react-slider/) for range inputs (easy tooltips) `npm i --save-dev react-slider @types/react-slider`

- [RC-Slider](https://www.npmjs.com/package/rc-slider/) for video controls (allows disabled keyboard) `npm i --save-dev rc-slider`

- [React Syntax Highlighter](https://www.npmjs.com/package/react-syntax-highlighter/) for code snippets (Prism) `npm i --save-dev react-syntax-highlighter` & types `npm i --save-dev @types/react-syntax-highlighter`

- removed Babel warning: `npm i --save-dev @babel/plugin-proposal-private-property-in-object --legacy-peer-deps`

## TODO:s

- [ ] more personal info
- [ ] Add projects + individual pages
- [ ] Add blog?
- [ ] Fix scrolling to content on refresh
- [ ] Video sliders scrubbing - cursor pointer
- [ ] Add bollards status icons (svgs)
