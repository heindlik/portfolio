<?php

require_once "corsHeaders.php";

$name = htmlspecialchars(stripslashes(trim($_POST["name"])));
$email = htmlspecialchars(stripslashes(trim($_POST["email"])));
$message = htmlspecialchars(stripslashes(trim($_POST["message"])));

$to = "web@heindlik.cz";
$subject = "heindlik.cz – Kontaktní formulář";
$txt = $message . "\n\n" . $name . ", " . $email;
$headers = "From: noreply@heindlik.cz\r\n";
$headers .= "Reply-To: {$email}\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/plain; charset=UTF-8\r\n";

// Suppress PHP error messages
error_reporting(0);

$response = array();
// TODO: replace noreply with existing email - its a fallback for undelivered emails
if (mail($to, $subject, $txt, $headers, "-f noreply@heindlik.cz")) {
  $response["success"] = true;
} else {
  $response["error"] = "Sending failed: " . error_get_last()['message'];
}

echo json_encode($response);
