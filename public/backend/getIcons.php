<?php

require_once "corsHeaders.php";

if (is_dir("../assets/icons")) {
  $files = glob("../assets/icons/*");
  $iconArray = array();

  foreach ($files as $file) {
    if (strtolower(pathinfo($file, PATHINFO_EXTENSION)) === "svg") {
      
      $name = pathinfo($file, PATHINFO_FILENAME);
      $xml = file_get_contents($file);

      $keywordsCS = array();
      preg_match('/<svg[^>]*?\sdata-keywords-cs=["\'](.*?)["\'][^>]*?>/', $xml, $matches);
      if (isset($matches[1])) {
        $keywordsCS = explode(' ', $matches[1]);
      }

      $keywordsEN = array();
      preg_match('/<svg[^>]*?\sdata-keywords-en=["\'](.*?)["\'][^>]*?>/', $xml, $matches);
      if (isset($matches[1])) {
        $keywordsEN = explode(' ', $matches[1]);
      }

      $icon = array(
        "name" => $name,
        "xml" => $xml,
        "keywordsCS" => $keywordsCS,
        "keywordsEN" => $keywordsEN
      );
      $iconArray[] = $icon;
    }
  }
}

echo json_encode($iconArray);
