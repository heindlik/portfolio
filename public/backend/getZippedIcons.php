<?php

require_once "corsHeaders.php";

$iconNames = isset($_POST["iconNames"]) ? $_POST["iconNames"] : null;

if (empty($iconNames)) {
  echo json_encode(array("error" => "Missing icon names."));
  exit();
}

$zipPath = "/assets/icons/icons_svg.zip";

// Create new zip file
$zip = new ZipArchive;
if ($zip->open(".." . $zipPath, ZipArchive::CREATE) === TRUE) {
  foreach ($iconNames as $iconName) {
    $filePath = "../assets/icons/" . $iconName . ".svg";
    $zip->addFile($filePath, basename($filePath));
  }
  $zip->close();
  echo json_encode(array("download" => $zipPath));
} else {
  echo json_encode(array("error" => "Failed to create zip file."));
}
