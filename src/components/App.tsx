import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { scrollToTop } from 'utils';
// Components
import Loading from 'components/loading/Loading';
import Alert from 'components/dialogs/alert/Alert';
import Confirm from 'components/dialogs/confirm/Confirm';
import Prompt from 'components/dialogs/prompt/Prompt';
import Header from 'components/header/Header';
import About from 'components/sections/about/About';
import Playground from 'components/sections/playground/Playground';
import Contact from 'components/sections/contact/Contact';
import Footer from 'components/footer/Footer';
// Styles
import 'components/sections/sections.scss';
// Interfaces
import { AlertInterface, ConfirmProps, PromptProps } from 'interfaces';
import Projects from './sections/projects/Projects';

export default function App() {

  /** Translations */
  const { t } = useTranslation();

  /** Vertical offset */
  const [offsetY, setOffsetY] = useState(window.scrollY);

  /** Highlighted link */
  const [activeSection, setActiveSection] = useState<string>('about');

  /** First render */
  useEffect(() => {
    // Allow transitions after mounted
    document.querySelector('.preload-transitions')?.classList.remove('preload-transitions');
    // Handle scroll
    const handleScroll = () => {
      // Offset for jump-top button
      if (window.scrollY > 20) {
        setOffsetY(window.scrollY);
      } else if (window.scrollY !== 0) {
        setOffsetY(0);
      }
      // Link highlight
      const sections = document.querySelectorAll('section');
      sections.forEach(section => {
        const rect = section.getBoundingClientRect();
        if (rect.top <= window.innerHeight / 2 && rect.bottom >= window.innerHeight / 2) {
          setActiveSection(section.id);
        }
      });
    };
    // Add scroll listener
    window.addEventListener('scroll', handleScroll);
    // CLeanup
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  /** Loading hides once fonts load */
  const [isLoading, setIsLoading] = useState<boolean>(true);
  useEffect(() => {
    if ('fonts' in document) {
      document.fonts.ready.finally(() => setIsLoading(false));
    } else { setIsLoading(false); }
  }, []);

  /** Alerts */
  const [alerts, setAlerts] = useState<AlertInterface[]>([]);

  /** Display new alert
   * @param alertMsg Message to display
   * @param type Alert type */
  const showAlert = useCallback((alertMsg: string, type?: 'success' | 'warning' | 'error') => {
    const newAlert = {
      timestamp: Date.now(),
      message: alertMsg,
      type
    };
    setAlerts(prevAlerts => [...prevAlerts, newAlert]);
  }, []);

  /** Hide alert (remove from array)
   * @param alertToHide Alert to hide */
  const hideAlert = useCallback((alertToHide: AlertInterface) => {
    setAlerts(prev => prev.filter(alert => alert.timestamp !== alertToHide.timestamp));
  }, []);

  /** Show confirm modal
   * @param newConfirmProps Partial props (other than default) */
  const showConfirm = (newConfirmProps: Partial<ConfirmProps>) => {
    setConfirmProps({ ...confirmProps, ...newConfirmProps, isOpen: true });
  };

  /** Close confirm modal (called from inside) */
  const onConfirmClose = () => {
    setConfirmProps({ ...confirmProps, isOpen: false });
  };

  // Default confirm props
  const [confirmProps, setConfirmProps] = useState<ConfirmProps>({
    isOpen: false, // Toggling visibility
    question: t('buttons.close'),
    buttons: [{
      label: 'OK',
      class: 'light'
    }],
    canEscape: true, // Escape key or Click outside dialog box
    onClose: onConfirmClose // Changes isOpen attribute
  });

  /** Show prompt modal
   * @param newPromptProps Partial props (other than default) */
  const showPrompt = (newPromptProps: Partial<PromptProps>) => {
    setPromptProps({ ...promptProps, ...newPromptProps, isOpen: true });
  };

  /** Close prompt modal (called from inside) */
  const onPromptClose = () => {
    setPromptProps({ ...promptProps, isOpen: false });
  };

  // Default prompt props
  const [promptProps, setPromptProps] = useState<PromptProps>({
    isOpen: false, // Toggling visibility
    prompt: t('buttons.close'),
    type: 'text',
    submitLabel: t('buttons.save'),
    onSubmit: (value: string) => console.log(value),
    canEscape: true, // Escape key or Click outside dialog box
    onClose: onPromptClose // Changes isOpen attribute
  });

  return (
    <>
      <Loading isLoading={isLoading} />

      <Confirm {...confirmProps} />
      <Prompt {...promptProps} />

      <ul className='alerts-container'>
        {alerts.map((alert: AlertInterface) =>
          <Alert key={alert.timestamp} alert={alert} hideAlert={hideAlert} />
        )}
      </ul>

      <Header activeSection={activeSection} />

      <main>
        <About />
        <Projects />
        <Playground showAlert={showAlert} showConfirm={showConfirm} showPrompt={showPrompt} />
        <Contact showAlert={showAlert} showConfirm={showConfirm} />
      </main>

      <Footer />

      {offsetY > 200 &&
        <div className='jump-top'>
          <button onClick={scrollToTop} title={t('buttons.goToTop')}>
            <svg viewBox='0 0 50 50'>
              <path d='M17,29L25,21L33,29' />
            </svg>
          </button>
        </div>
      }
    </>
  );
}
