import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
// Assets
import * as Icons from 'assets/icons/icons';
// Styles
import './Alert.scss';
// Interfaces
import { AlertInterface } from 'interfaces';
interface AlertProps {
  alert: AlertInterface;
  hideAlert: (alert: AlertInterface) => void;
}

export default function Alert(props: AlertProps) {

  /** Translations */
  const { t } = useTranslation();

  /** Props */
  const { alert, hideAlert } = props;

  /** Lifespan states */
  const [isHiding, setIsHiding] = useState<boolean>(false);
  const [isPaused, setIsPaused] = useState<boolean>(false);
  const [remainingTime, setRemainingTime] = useState<number>(6000); // 6 seconds

  /** Start hiding transition & remove alert from parent array */
  const callHideAlert = useCallback(() => {
    setIsHiding(true);
    setTimeout(() => {
      hideAlert(alert);
    }, 400);
  }, [alert, hideAlert]);

  /** Hide alert after lifespan */
  useEffect(() => {
    if (isPaused) { return; }
    const startTime = Date.now();
    // Hide alert after remaining time
    const timer = setTimeout(() => {
      callHideAlert();
    }, remainingTime);
    // Update every 100ms
    const interval = setInterval(() => {
      const elapsedTime = Date.now() - startTime;
      setRemainingTime(prevTime => Math.max(0, prevTime - elapsedTime));
    }, 100);
    // Cleanup
    return () => {
      clearTimeout(timer);
      clearInterval(interval);
    };
  }, [isPaused, remainingTime, callHideAlert]); // Re-run effect if these dependencies change

  return (
    <li className={`alert ${alert.type} ${isHiding && 'hiding'}`}
      onMouseOver={() => setIsPaused(true)}
      onFocus={() => setIsPaused(true)}
      onMouseOut={() => setIsPaused(false)}
      onBlur={() => setIsPaused(false)}>
      <div>
        {!alert.type && <Icons.BadgeInfo className="icon" aria-hidden="true" />}
        {alert.type === 'success' && <Icons.BadgeSuccess className="icon" aria-hidden="true" />}
        {alert.type === 'error' && <Icons.BadgeError className="icon" aria-hidden="true" />}
        {alert.type === 'warning' && <Icons.BadgeWarning className="icon" aria-hidden="true" />}
        <p>{alert.message || t('playground.alerts.emptyAlertMsg')}</p>
        <button aria-label={t('system.hideAlert')}
          onClick={() => callHideAlert()}
          onFocus={() => setIsPaused(true)}
          onBlur={() => setIsPaused(false)}>
          <Icons.Cross aria-hidden="true" />
        </button>
        <div className="lifespan" style={{ animationPlayState: isPaused ? 'paused' : 'running' }} />
      </div>
    </li>
  );
}