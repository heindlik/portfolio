import { useCallback, useEffect, useRef } from 'react';
import { fixBody } from 'utils';
// Styles
import './Confirm.scss';
// Interface
import { ConfirmProps } from 'interfaces';

export default function Confirm(props: ConfirmProps) {

  /** Props */
  const { isOpen, question, buttons, canEscape, onClose } = props;

  /** Dialog element */
  const confirmRef = useRef<HTMLDialogElement | null>(null);

  /** Close modal dialog & call outside function */
  const close = useCallback(() => {
    confirmRef.current?.close();
    onClose();
  }, [onClose]);

  /** Listen to isOpen value */
  useEffect(() => {
    if (isOpen) {
      fixBody(true);
      confirmRef.current?.showModal();
    } else {
      fixBody(false);
      close();
    }
  }, [isOpen, close]);

  useEffect(() => {
    const confirmEl = confirmRef.current;
    // Clicking outside the dialog box
    const confirmOutClick = (e: MouseEvent) => {
      if (e.button === 0 && confirmEl) {
        const dialogDimensions = confirmEl.getBoundingClientRect();
        if (
          canEscape &&
          (e.clientX < dialogDimensions.left ||
            e.clientX > dialogDimensions.right ||
            e.clientY < dialogDimensions.top ||
            e.clientY > dialogDimensions.bottom
          )) {
          close();
        }
      }
    };
    window.addEventListener('mousedown', confirmOutClick);

    // Escape key
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        e.preventDefault();
        if (canEscape) { close(); }
      }
    };
    confirmEl?.addEventListener('keydown', handleKeyDown);

    // Remove listeners
    return () => {
      window.removeEventListener('mousedown', confirmOutClick);
      confirmEl?.removeEventListener('keydown', handleKeyDown);
    };
  }, [canEscape, close]);

  return (
    <dialog ref={confirmRef} id="confirm">
      <p>{question}</p>
      <div className="buttons">
        {buttons.map((btn, index) =>
          <button key={index} className={`btn default ${btn.class || ''}`}
            onClick={() => { btn.action?.(); close(); }}>
            {btn.label}
          </button>
        )}
      </div>
    </dialog>
  );
}
