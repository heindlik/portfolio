import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { fixBody } from 'utils';
// Styles
import './Prompt.scss';
// Interface
import { PromptProps } from 'interfaces';

export default function Prompt(props: PromptProps) {

  const { t } = useTranslation();

  /** Props */
  const { isOpen, prompt, type, options, submitLabel, onSubmit, canEscape, onClose } = props;

  /** Dialog element */
  const promptRef = useRef<HTMLDialogElement | null>(null);

  /** Returned value */
  const [value, setValue] = useState<string>(''); // Stringified value

  /** Close modal dialog & call outside function */
  const close = useCallback(() => {
    promptRef.current?.close();
    onClose();
  }, [onClose]);

  /** Listen to isOpen value */
  useEffect(() => {
    if (isOpen) {
      fixBody(true);
      promptRef.current?.showModal();
      // If init & options available, select first, else select previously selected
      setValue(prevVal => (!prevVal && options) ? options[0].value : prevVal);
    } else {
      fixBody(false);
      close();
    }
  }, [isOpen, options, close]);

  useEffect(() => {
    const promptEl = promptRef.current;
    // Clicking outside the dialog box
    const promptOutClick = (e: MouseEvent) => {
      if (e.button === 0 && promptEl) {
        const dialogDimensions = promptEl.getBoundingClientRect();
        if (
          canEscape &&
          (e.clientX < dialogDimensions.left ||
            e.clientX > dialogDimensions.right ||
            e.clientY < dialogDimensions.top ||
            e.clientY > dialogDimensions.bottom
          )) {
          close();
        }
      }
    };
    window.addEventListener('mousedown', promptOutClick);

    // Escape key
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        e.preventDefault();
        if (canEscape) { close(); }
      }
    };
    promptEl?.addEventListener('keydown', handleKeyDown);

    // Remove listeners
    return () => {
      window.removeEventListener('mousedown', promptOutClick);
      promptEl?.removeEventListener('keydown', handleKeyDown);
    };
  }, [canEscape, close]);

  return (
    <dialog ref={promptRef} id="prompt">
      <p>{prompt}</p>

      {type === 'text' &&
        <input type="text" value={value} onChange={(e) => setValue(e.target.value)} />
      }

      {type === 'number' &&
        <input type="number" value={value} onChange={(e) => setValue(e.target.value)} />
      }

      {type === 'radios' && !!options &&
        options.map((option, index) =>
          <label key={index}>
            <input type="radio" key={index} name="prompt-radio" value={option.value}
              checked={value === option.value} onChange={(e) => setValue(e.target.value)} />
            {option.label}
          </label>
        )
      }

      {type === 'select' && !!options &&
        <select value={value} onChange={(e) => setValue(e.target.value)}>
          {options.map((option, index) =>
            <option key={index} value={option.value}>{option.label}</option>
          )}
        </select>
      }

      <div className="buttons">
        <button className="btn default light" onClick={close}>
          {t('buttons.storno')}
        </button>
        <button className="btn default" onClick={() => { onSubmit(value); close(); }} disabled={!value}>
          {submitLabel}
        </button>
      </div>
    </dialog >
  );
}
