import { useTranslation } from 'react-i18next';
// Styles
import './Footer.scss';
// Assets
import * as Icons from 'assets/icons/icons';

export default function Footer() {

  /** Translations */
  const { t } = useTranslation();

  return (
    <footer>
      <div className="links">
        <a href="https://gitlab.com/heindlik/" target="_blank" rel="noreferrer noopener"
          aria-label={t('system.openInNewTab', { linkName: 'GitLab' })}>
          GitLab
          <Icons.ArrowTopRight aria-hidden="true" />
        </a>
      </div>
    </footer >
  );
}
