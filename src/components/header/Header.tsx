import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { fixBody } from 'utils';
// Assets
import { ReactComponent as LogoSVG } from 'assets/logo.svg';
import * as Icons from 'assets/icons/icons';
// Styles
import './Header.scss';
// Interfaces
interface HeaderProps {
  activeSection: string;
}

export default function Header(props: HeaderProps) {

  /** Props */
  const { activeSection } = props;

  /** Translations */
  const { i18n, t } = useTranslation();

  /** Mobile menu */
  const mobileRef = useRef<HTMLSpanElement>(null);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState<boolean>(false);
  const [isMobileMenuHiding, setIsMobileMenuHiding] = useState<boolean>(false); // For fade-outs

  /** Show/hide mobile menu
   * @param doOpen Force open menu */
  const toggleMobileMenu = useCallback((doOpen?: boolean) => {
    if (window.innerWidth <= 700 && (doOpen || (doOpen === undefined && !isMobileMenuOpen))) {
      setIsMobileMenuOpen(true);
      fixBody(true);
    } else if (isMobileMenuOpen) {
      setIsMobileMenuHiding(true);
      fixBody(false);
      setTimeout(() => {
        setIsMobileMenuHiding(false);
        setIsMobileMenuOpen(false);
      }, 200);
    }
  }, [isMobileMenuOpen]);

  /** Handle resize */
  useEffect(() => {
    const handleResize = () => {
      if (isMobileMenuOpen && window.innerWidth > 700) {
        toggleMobileMenu(false);
      }
    };
    // Assign listener
    window.addEventListener('resize', handleResize);
    // Cleanup
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [isMobileMenuOpen, toggleMobileMenu]);


  /** Color mode menu */
  const colorModeBtnRef = useRef<HTMLButtonElement>(null);
  const colorModeMenuRef = useRef<HTMLDivElement>(null);
  const [isColorModeMenuOpen, setIsColorModeMenuOpen] = useState<boolean>(false);

  /** Color mode switch */
  const [colorMode, setColorMode] = useState<string>(localStorage.getItem('mode') || 'auto');
  // Assign proper class
  const switchColorMode = useCallback(() => {
    // Target state != colorMode (switchTo is specific mode, colorMode can be 'auto')
    let switchTo;
    if (colorMode === 'auto') {
      const systemDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
      switchTo = systemDark ? 'dark' : 'light';
    } else {
      switchTo = colorMode;
    }
    document.documentElement.classList.remove('auto', 'light', 'dark', 'contrast');
    document.documentElement.classList.add(switchTo);
  }, [colorMode]);
  // React to change & save to localStorage
  useEffect(() => {
    switchColorMode();
    localStorage.setItem('mode', colorMode);
  }, [colorMode, switchColorMode]);
  // If auto, switch on system change
  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', () => {
    if (colorMode === 'auto') {
      switchColorMode();
    }
  });


  /** Translations */
  const [language, setLanguage] = useState<string>(localStorage.getItem('language') || 'cs');
  useEffect(() => {
    i18n.changeLanguage(language);
    // Change html lang attribute
    document.documentElement.setAttribute('lang', language);
    // Change title
    document.title = i18n.t('title');
    // Change meta description
    const metaDescription = document.querySelector('meta[name="description"]');
    if (metaDescription) {
      metaDescription.setAttribute('content', i18n.t('metaDesc'));
    }
    // Save to localStorage
    localStorage.setItem('language', language);
  }, [language, i18n]);


  /** Toggling menus on click */
  useEffect(() => {
    const mobile = mobileRef.current;
    const colorModeBtn = colorModeBtnRef.current;
    const colorModeMenu = colorModeMenuRef.current;
    // Handle click
    const handleClick = (e: MouseEvent) => {
      // Color mode menu
      if (colorModeBtn && colorModeBtn.contains(e.target as Node)) {
        setIsColorModeMenuOpen(prev => prev = !prev);
      } else if (colorModeMenu && !colorModeMenu.contains(e.target as Node)) {
        setIsColorModeMenuOpen(false);
      }
    };
    // Handle keydown & hide menus on Escape
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Escape' && (isMobileMenuOpen || isColorModeMenuOpen)) {
        toggleMobileMenu(false);
        setIsColorModeMenuOpen(false);
      }
      // Disable focusables in the bg when menu is open
      if (isMobileMenuOpen && e.key === 'Tab') {
        if (!mobile) { return; }
        const focusables = mobile.querySelectorAll<HTMLElement>('button, a, [tabindex]:not([tabindex="-1"])');
        const firstFocusable = focusables[0];
        const lastFocusable = focusables[focusables.length - 1];
        if (!e.shiftKey && document.activeElement === lastFocusable) {
          firstFocusable.focus();
          e.preventDefault();
        } else if (e.shiftKey && document.activeElement === firstFocusable) {
          lastFocusable.focus();
          e.preventDefault();
        }
      }
    };
    // Listeners
    window.addEventListener('click', handleClick);
    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('click', handleClick);
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [isMobileMenuOpen, toggleMobileMenu, isColorModeMenuOpen]);


  return (
    <header>
      <nav>

        {/* Mobile menu */}
        <span ref={mobileRef} className="mobile">
          <button className="menu-btn" aria-expanded={!isMobileMenuHiding && isMobileMenuOpen} onClick={() => toggleMobileMenu()}
            title={isMobileMenuOpen ? t('system.closeMenu') : t('system.openMenu')}>
            <div className="menu-btn-burger" />
          </button>
          <div className={`menu-overlay ${isMobileMenuHiding && 'hiding'} ${(isMobileMenuOpen) && 'visible'}`}
            onClick={() => toggleMobileMenu(false)} aria-hidden="true" />
          <div className={`menu ${isMobileMenuHiding && 'hiding'}`} aria-hidden={!isMobileMenuOpen}>
            <a href="/#" onClick={() => toggleMobileMenu(false)} aria-label={`${t('system.goTo')} ${t('about.title')}`}>
              <LogoSVG className="logo" />
            </a>
            <a href="/#projects" onClick={() => toggleMobileMenu(false)} aria-label={`${t('system.goTo')} ${t('projects.title')}`}>
              {t('projects.title')}
            </a>
            <a href="/#playground" onClick={() => toggleMobileMenu(false)} aria-label={`${t('system.goTo')} ${t('playground.title')}`}>
              {t('playground.title')}
            </a>
            <a href="/#contact" onClick={() => toggleMobileMenu(false)} aria-label={`${t('system.goTo')} ${t('contact.title')}`}>
              {t('contact.title')}
            </a>
          </div>
        </span>

        {/* Navigation */}
        <a className={activeSection === 'about' ? 'active' : ''} href="/#about"
          aria-label={`${t('system.goTo')} ${t('about.title')}`}>
          <LogoSVG className="logo" />
        </a>
        <a className={activeSection === 'projects' ? 'active' : ''} href="/#projects"
          aria-label={`${t('system.goTo')} ${t('projects.title')}`}>
          {t('projects.title')}
        </a>
        <a className={activeSection === 'playground' ? 'active' : ''} href="/#playground"
          aria-label={`${t('system.goTo')} ${t('playground.title')}`}>
          {t('playground.title')}
        </a>
        <a className={activeSection === 'contact' ? 'active' : ''} href="/#contact"
          aria-label={`${t('system.goTo')} ${t('contact.title')}`}>
          {t('contact.title')}
        </a>

        {/* Color mode */}
        <span className="color-mode">
          <button ref={colorModeBtnRef} className="color-mode-btn" aria-expanded={isColorModeMenuOpen} title={t('system.colorMode')}>
            <Icons.Palette aria-hidden="true" />
          </button>
          <div ref={colorModeMenuRef} className="color-mode-menu" aria-hidden={!isColorModeMenuOpen}>
            <div className="scrollable">
              <p>{t('system.colorMode')}</p>
              <hr />
              <label>
                <input type="radio" value="auto" checked={colorMode === 'auto'} onChange={() => setColorMode('auto')} />
                {t('system.auto')}
              </label>
              <label>
                <input type="radio" value="light" checked={colorMode === 'light'} onChange={() => setColorMode('light')} />
                {t('system.light')}
              </label>
              <label>
                <input type="radio" value="dark" checked={colorMode === 'dark'} onChange={() => setColorMode('dark')} />
                {t('system.dark')}
              </label>
              <label>
                <input type="radio" value="contrast" checked={colorMode === 'contrast'} onChange={() => setColorMode('contrast')} />
                {t('system.contrast')}
              </label>
            </div>
          </div>
        </span>

        {/* Language */}
        <span className="lang">
          <button className="lang-btn" onClick={() => setLanguage(language === 'cs' ? 'en' : 'cs')}
            title={t('system.langChange')}>
            <Icons.Globe aria-hidden="true" />
          </button>
        </span>

      </nav>
    </header >
  );
}
