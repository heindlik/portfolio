import { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
// Assets
import * as Icons from 'assets/icons/icons';
// Styles
import './Loading.scss';
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
import { fixBody } from 'utils';

export default function Loading(props: { isLoading: boolean }) {

  /** Translations */
  const { t } = useTranslation();

  const [isLoading, setIsLoading] = useState<boolean>(true); // Show/hide loading
  const [isHiding, setIsHiding] = useState<boolean>(false); // Show/hide fade-out
  const [isTooLong, setIsTooLong] = useState(false); // Show/hide timeout message

  /** Timeouts for proper clearing */
  const hidingTimeoutRef = useRef<NodeJS.Timeout | null>(null);
  const tooLongTimeoutRef = useRef<NodeJS.Timeout | null>(null);

  /** React to parent's command */
  useEffect(() => {
    if (!props.isLoading) {
      fixBody(false);
      // Clear timeout
      if (tooLongTimeoutRef.current) { clearTimeout(tooLongTimeoutRef.current); }
      // Initiate fadeout
      setIsHiding(true);
      // Delay isLoading until isHiding is complete
      hidingTimeoutRef.current = setTimeout(() => {
        setIsHiding(false);
        setIsLoading(false);
      }, 200);
    } else {
      fixBody(true);
      // Clear hiding timeout
      if (hidingTimeoutRef.current) { clearTimeout(hidingTimeoutRef.current); }
      setIsHiding(false);
      setIsLoading(true);
      // Start 10s timeout
      tooLongTimeoutRef.current = setTimeout(() => {
        setIsTooLong(true);
      }, 10000);
    }

    // Cleanup
    return () => {
      if (tooLongTimeoutRef.current) { clearTimeout(tooLongTimeoutRef.current); }
      if (hidingTimeoutRef.current) { clearTimeout(hidingTimeoutRef.current); }
    };
  }, [props.isLoading]);

  return (
    <div className={`loading-overlay ${isLoading} ${isTooLong && 'too-long'} ${isHiding && 'hiding'}`}
      aria-label={t('system.loading')} role="status">
      <ProgressRing behavior="grow-shrink" />

      <div className="accordion">
        <div className="timeout-msg">
          <p>{t('system.loadingTooLong')}</p>
          <button className="btn default light reload-btn" onClick={() => window.location.reload()}>
            <Icons.Reload />
            {t('system.reloadPage')}
          </button>
        </div>
      </div>
    </div>
  );
}