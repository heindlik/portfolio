import { useTranslation } from 'react-i18next';
// Styles
import './About.scss';

export default function About() {

  /** Translations */
  const { t } = useTranslation();

  return (
    <section id="about">
      <img className="hero" src="/assets/images/personal/summit-600.jpg" alt={t('about.heroImageAlt')} />
      <h1>{t('about.hey')}</h1>
      <h1 className="highlight">
        {t('about.iCraft')}
        <span className="underscore">_</span>
      </h1>
      <p>
        {t('about.myNameIs')} <b>Martin Heindl</b>{t('about.iSpecialize')} <b>front-end</b> {t('about.webDevelopment')}
      </p>
    </section>
  );
}