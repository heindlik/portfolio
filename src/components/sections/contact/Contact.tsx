import { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { blockEnter } from 'utils';
import env from 'env';
import axios from 'axios';
// Components
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
// Assets
import * as Icons from 'assets/icons/icons';
// Styles
import './Contact.scss';
// Interfaces
import { ConfirmProps } from 'interfaces';
interface ContactProps {
  showAlert: (alertMsg: string) => void;
  showConfirm: (newConfirmProps: Partial<ConfirmProps>) => void;
}

export default function Contact(props: ContactProps) {

  /** Props */
  const { showAlert, showConfirm } = props;

  /** Translations */
  const { t } = useTranslation();

  /** Form */
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  // Reset e-mail validity
  const emailInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    emailRef.current?.setCustomValidity('');
    emailRef.current?.reportValidity();
    setEmail(e.target.value);
  };
  // Email input ref
  const emailRef = useRef<HTMLInputElement>(null);
  const [message, setMessage] = useState<string>('');

  /** Process variables */
  const [isSending, setIsSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [isHiding, setIsHiding] = useState(false);

  /** Validate inputs & send message */
  const submitMessage = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (isSending || success) {
      return;
    }
    // Validate inputs
    const emailFormat = /^(?!\.)[a-zA-Z0-9._%+-]+@(?!-)[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!name.length || !email.length || !message.length) {
      showAlert(t('contact.fillAllInputs'));
      return;
    } else if (!email.match(emailFormat)) {
      emailRef.current?.setCustomValidity(t('contact.invalidEmail'));
      emailRef.current?.reportValidity();
      return;
    }
    // Show confirm popup
    showConfirm({
      question: t('contact.confirmSend'),
      buttons: [{
        label: t('buttons.storno'),
        class: 'light'
      }, {
        label: t('contact.send'),
        action: () => {
          setIsSending(true);
          setTimeout(() => {
            sendMessage(name, email, message);
          }, 1000);
        }
      }]
    });
  };

  /** Send message to BE
   * @param name User's name
   * @param email User's e-mail
   * @param message User's message */
  const sendMessage = (name: string, email: string, message: string) => {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('message', message);
    axios.post(env.api + `contactForm.php?t=${Date.now()}`, formData).then(r => {
      const response = r.data;
      setIsSending(false);
      if (response.success) {
        setSuccess(true);
      } else {
        showAlert(t('contact.sendingFailed'));
        console.error(response.error);
        setIsHiding(true);
        setTimeout(() => {
          setIsHiding(false);
        }, 200);
      }
    }).catch(e => {
      setIsSending(false);
      showAlert(t('contact.sendingFailed'));
      console.error(e);
    });
  };

  return (
    <section id="contact">
      <h2>
        <a href="#contact" aria-label={`${t('system.goTo')} ${t('contact.title')}`}>
          {t('contact.title')}
        </a>
      </h2>

      <form onSubmit={submitMessage}>
        <p className="description">{t('contact.description')}</p>

        <div className="inputs">
          <input name="name" type="text" autoComplete="name"
            placeholder={t('contact.yourNameSurname')} title={t('contact.yourNameSurname')}
            value={name}
            onInput={(e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value)}
            onKeyDown={blockEnter}
            disabled={isSending || success} required />

          <input ref={emailRef} name="email" type="email" autoComplete="email" spellCheck={false}
            placeholder={t('contact.yourEmail')} title={t('contact.yourEmailToGetBack')}
            value={email}
            onInput={emailInput}
            onKeyDown={blockEnter}
            disabled={isSending || success} required />
        </div>

        <textarea name="message" placeholder={t('contact.message')} title={t('contact.message')}
          value={message}
          onInput={(e: React.ChangeEvent<HTMLTextAreaElement>) => setMessage(e.target.value)}
          disabled={isSending || success} required />

        <button className="btn default" type="submit" disabled={isSending || success}>
          <Icons.PaperPlane aria-hidden="true" />
          <span>{t('contact.send')}</span>
        </button>

        {(isSending || success || isHiding) && <div className={`overlay ${isHiding && 'hiding'}`}>
          <ProgressRing progress={success ? 100 : 0} behavior={success ? 'progressive' : 'grow-shrink'} />
          {!success && <p>{t('contact.sending')}</p>}
          {success && <p>{t('contact.thanksForMessage')}<br />{t('contact.willReply')}</p>}
        </div>}
      </form>
    </section>
  );
}