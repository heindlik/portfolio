import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
// Components
import InputsPlayground from './inputs-playground/InputsPlayground';
import LoadingsPlayground from './loadings-playground/LoadingsPlayground';
import IconsPlayground from './icons-playground/IconsPlayground';
import TablesPlayground from './tables-playground/TablesPlayground';
import AlertsPlayground from './alerts-playground/AlertsPlayground';
import MediaPlayground from './media-playground/MediaPlayground';
// Assets
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
// Styles
import './Playground.scss';
// Interfaces
import { ConfirmProps, PromptProps } from 'interfaces';
interface PlaygroundProps {
  showAlert: (alertMsg: string) => void;
  showConfirm: (newConfirmProps: Partial<ConfirmProps>) => void;
  showPrompt: (newPromptProps: Partial<PromptProps>) => void;
}
export default function Playground(props: PlaygroundProps) {

  /** Props */
  const { showAlert, showConfirm, showPrompt } = props;

  /** Translations */
  const { t, i18n } = useTranslation();

  /** Tablist */
  const tabListRef = useRef<HTMLUListElement>(null);
  const [activeTab, setActiveTab] = useState<string>(localStorage.getItem('activeTab') || 'inputs');
  const [lastTab, setLastTab] = useState<string>(localStorage.getItem('activeTab') || 'inputs');

  /** Active underline position from left (of the screen) and right (of the tablist) */
  const [underlineLeft, setUnderlineLeft] = useState(0);
  const [underlineRight, setUnderlineRight] = useState(0);
  const underlineTimeout = useRef<NodeJS.Timeout | null>(null);

  /** Idle state hides the underline */
  const [idle, setIdle] = useState(true);
  const idleTimeout = useRef<NodeJS.Timeout | null>(null);

  /** Tabs display mode */
  const [isCompact, setIsCompact] = useState<boolean>(false);

  /** Handle window resize */
  const handleResize = useCallback(() => {
    const tabList = tabListRef.current;
    const activeTabBtn = document.getElementById(activeTab + '-tab');
    if (!tabList || !activeTabBtn) { return; }
    setIsCompact(tabList.scrollWidth > tabList.clientWidth);
    setUnderlineLeft(activeTabBtn.offsetLeft);
    setUnderlineRight(tabList.clientWidth - activeTabBtn.offsetLeft - activeTabBtn.offsetWidth);
  }, [activeTab]);
  useEffect(() => {
    const tabListCurrent = tabListRef.current;
    // Initial resize
    handleResize();
    // Setup resize observer
    const resizeObserver = new ResizeObserver(handleResize);
    // Start observing
    if (tabListCurrent) {
      resizeObserver.observe(tabListCurrent);
    }
    // Cleanup
    return () => {
      if (tabListCurrent) {
        resizeObserver.unobserve(tabListCurrent);
      }
    };
  }, [handleResize]);
  // Trigger resize on language change
  useEffect(() => {
    handleResize();
  }, [i18n.language, handleResize]);

  /** Handle active tab change */
  useEffect(() => {
    const tabList = tabListRef.current;
    const activeTabBtn = document.getElementById(activeTab + '-tab');
    const lastTabBtn = document.getElementById(lastTab + '-tab');
    if (!tabList || !activeTabBtn || !lastTabBtn) { return; }
    // Reset timeouts
    if (underlineTimeout.current) { clearTimeout(underlineTimeout.current); }
    if (idleTimeout.current) { clearTimeout(idleTimeout.current); }
    // Underline goes left
    if (lastTabBtn.offsetLeft > activeTabBtn.offsetLeft) {
      setUnderlineLeft(activeTabBtn.offsetLeft);
      underlineTimeout.current = setTimeout(() => {
        setUnderlineRight(tabList.clientWidth - activeTabBtn.offsetLeft - activeTabBtn.offsetWidth);
        idleTimeout.current = setTimeout(() => setIdle(true), 200); // Waits var(--speed) for animation to finish
      }, 50);
      // Underline goes right
    } else {
      setUnderlineRight(tabList.clientWidth - activeTabBtn.offsetLeft - activeTabBtn.offsetWidth);
      underlineTimeout.current = setTimeout(() => {
        setUnderlineLeft(activeTabBtn.offsetLeft);
        idleTimeout.current = setTimeout(() => setIdle(true), 200); // Waits var(--speed) for animation to finish
      }, 50);
    }
    setLastTab(activeTab);
    // Save active tab to localStorage
    localStorage.setItem('activeTab', activeTab);
  }, [activeTab, lastTab, isCompact]);

  return (
    <section id="playground">
      <h2>
        <a href="#playground" aria-label={`${t('system.goTo')} ${t('playground.title')}`}>
          {t('playground.title')}
        </a>
      </h2>
      <p className="description">{t('playground.description')}</p>

      {/* Placeholder for width calculation */}
      <ul ref={tabListRef} className="playground-tablist hidden" role="tablist" aria-hidden="true">
        <li><button disabled><svg /><span>{t('playground.inputs.title')}</span></button></li>
        <li><button disabled><svg /><span>{t('playground.loadings.title')}</span></button></li>
        <li><button disabled><svg /><span>{t('playground.icons.title')}</span></button></li>
        <li><button disabled><svg /><span>{t('playground.media.title')}</span></button></li>
        <li><button disabled><svg /><span>{t('playground.tables.title')}</span></button></li>
        <li><button disabled><svg /><span>{t('playground.alerts.title')}</span></button></li>
      </ul>

      {/* Actual tabs */}
      <ul className={`playground-tablist ${isCompact && 'compact-view'}`} role="tablist">
        <li role="presentation">
          <button role="tab" id="inputs-tab" title={isCompact ? t('playground.inputs.title') : ''} aria-controls="inputs-panel"
            aria-selected={activeTab === 'inputs'} disabled={activeTab === 'inputs'}
            onClick={() => { setIdle(false); setActiveTab('inputs'); }}>
            <AnimatedIcons.Inputs className="inputs-icon" />
            <span>{t('playground.inputs.title')}</span>
          </button>
        </li>
        <li role="presentation">
          <button role="tab" id="loadings-tab" title={isCompact ? t('playground.loadings.title') : ''} aria-controls="loadings-panel"
            aria-selected={activeTab === 'loadings'} disabled={activeTab === 'loadings'}
            onClick={() => { setIdle(false); setActiveTab('loadings'); }}>
            <AnimatedIcons.Loadings className="loadings-icon" />
            <span>{t('playground.loadings.title')}</span>
          </button>
        </li>
        <li role="presentation">
          <button role="tab" id="icons-tab" title={isCompact ? t('playground.icons.title') : ''} aria-controls="icons-panel"
            aria-selected={activeTab === 'icons'} disabled={activeTab === 'icons'}
            onClick={() => { setIdle(false); setActiveTab('icons'); }}>
            <AnimatedIcons.Icons className="icons-icon" />
            <span>{t('playground.icons.title')}</span>
          </button>
        </li>
        <li role="presentation">
          <button role="tab" id="media-tab" title={isCompact ? t('playground.media.title') : ''} aria-controls="media-panel"
            aria-selected={activeTab === 'media'} disabled={activeTab === 'media'}
            onClick={() => { setIdle(false); setActiveTab('media'); }}>
            <AnimatedIcons.Media className="media-icon" />
            <span>{t('playground.media.title')}</span>
          </button>
        </li>
        <li role="presentation">
          <button role="tab" id="tables-tab" title={isCompact ? t('playground.tables.title') : ''} aria-controls="tables-panel"
            aria-selected={activeTab === 'tables'} disabled={activeTab === 'tables'}
            onClick={() => { setIdle(false); setActiveTab('tables'); }}>
            <AnimatedIcons.Tables className="tables-icon" />
            <span>{t('playground.tables.title')}</span>
          </button>
        </li>
        <li role="presentation">
          <button role="tab" id="alerts-tab" title={isCompact ? t('playground.alerts.title') : ''} aria-controls="alerts-panel"
            aria-selected={activeTab === 'alerts'} disabled={activeTab === 'alerts'}
            onClick={() => { setIdle(false); setActiveTab('alerts'); }}>
            <AnimatedIcons.Alerts className="alerts-icon" />
            <span>{t('playground.alerts.title')}</span>
          </button>
        </li>

        <div className={`underline ${idle ? 'hidden' : ''}`} aria-hidden={true}
          style={{ left: `${underlineLeft}px`, right: `${underlineRight}px` }} />
      </ul>

      <div role="tabpanel" id="inputs-panel" aria-labelledby="inputs-tab" hidden={activeTab !== 'inputs'}>
        <InputsPlayground isCompact={isCompact} showAlert={showAlert} />
      </div>

      <div role="tabpanel" id="loadings-panel" aria-labelledby="loadings-tab" hidden={activeTab !== 'loadings'}>
        <LoadingsPlayground isCompact={isCompact} />
      </div>

      <div role="tabpanel" id="icons-panel" aria-labelledby="icons-tab" hidden={activeTab !== 'icons'}>
        <IconsPlayground isCompact={isCompact} showAlert={showAlert} />
      </div>

      <div role="tabpanel" id="media-panel" aria-labelledby="media-tab" hidden={activeTab !== 'media'}>
        <MediaPlayground isCompact={isCompact} />
      </div>

      <div role="tabpanel" id="tables-panel" aria-labelledby="tables-tab" hidden={activeTab !== 'tables'}>
        <TablesPlayground isCompact={isCompact} showAlert={showAlert} showConfirm={showConfirm} showPrompt={showPrompt} />
      </div>

      <div role="tabpanel" id="alerts-panel" aria-labelledby="alerts-tab" hidden={activeTab !== 'alerts'}>
        <AlertsPlayground isCompact={isCompact} showAlert={showAlert} />
      </div>

    </section>
  );
}