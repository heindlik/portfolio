import { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
// Assets
import * as Icons from 'assets/icons/icons';
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
// Styles
import './AlertsPlayground.scss';
// Interfaces
interface AlertsPlaygroundProps {
  isCompact: boolean;
  showAlert: (alertMsg: string, type?: 'success' | 'warning' | 'error') => void;
}

export default function AlertsPlayground(props: AlertsPlaygroundProps) {

  /** Props */
  const { isCompact, showAlert } = props;

  /** Translations */
  const { t } = useTranslation();

  /** Alert message input */
  const [alertMessage, setAlertMessage] = useState<string>('');

  /** Alert type (empty for neutral) */
  const [alertType, setAlertType] = useState<'success' | 'warning' | 'error'>();

  /** Clear input */
  const inputRef = useRef<HTMLInputElement>(null);
  const clearInput = () => {
    setAlertMessage('');
    inputRef.current?.focus();
  };

  /** Play bell animation on submit */
  const [animate, setAnimate] = useState(false);

  /** Submit form & show alert */
  const submitAlert = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Reset animation, play it again, remove (to avoid animation on tab change)
    setAnimate(false);
    setTimeout(() => setAnimate(true), 0);
    setTimeout(() => setAnimate(false), 1500);
    // Show alert
    showAlert(alertMessage, alertType);
  };

  return (
    <>
      {isCompact && <h3>{t('playground.alerts.title')}</h3>}
      <p className="description">{t('playground.alerts.description')}</p>

      <form className="toolbar alerts-toolbar" onSubmit={submitAlert}>
        <span className="input-container">
          <input ref={inputRef} type="text" spellCheck={false} placeholder={t('playground.alerts.alertText')}
            value={alertMessage} onInput={(e: React.ChangeEvent<HTMLInputElement>) => setAlertMessage(e.target.value)} />
          {!!alertMessage.length &&
            <button className="icon button" type="button"
              onClick={clearInput} tabIndex={-1} aria-label={t('system.clearInput')}>
              <Icons.CrossSmall aria-hidden="true" />
            </button>
          }
        </span>

        <select value={alertType} title={t('playground.alerts.alertType')}
          onChange={(e: React.ChangeEvent<HTMLSelectElement>) => setAlertType(e.target.value as 'success' | 'warning' | 'error')}>
          <option>{t('playground.alerts.neutral')}</option>
          <option value="success">{t('playground.alerts.success')}</option>
          <option value="warning">{t('playground.alerts.warning')}</option>
          <option value="error">{t('playground.alerts.error')}</option>
        </select>

        <button className="btn default show-alert-btn" type="submit" aria-label={t('playground.alerts.showAlert')}>
          <AnimatedIcons.Alerts className={animate ? 'animate' : ''} aria-hidden="true" />
          <span>{t('playground.alerts.show')}</span>
        </button>
      </form>
    </>
  );
}