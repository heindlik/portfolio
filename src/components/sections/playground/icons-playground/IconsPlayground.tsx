import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { fixBody, normalize } from 'utils';
import axios from 'axios';
import env from 'env';
// Components
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
import * as Icons from 'assets/icons/icons';
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
import { ICON_NAMES } from './iconNames';
// Styles
import './IconsPlayground.scss';
// Interfaces
interface IconsPlaygroundProps {
  isCompact: boolean;
  showAlert: (alertMsg: string) => void;
}
interface Icon {
  name: string;
  xml: string;
  keywordsCS?: string[];
  keywordsEN?: string[];
}

export default function IconsPlayground(props: IconsPlaygroundProps) {

  /** Props */
  const { isCompact, showAlert } = props;

  /** Translations */
  const { i18n, t } = useTranslation();

  /** Icons */
  const [icons, setIcons] = useState<Icon[]>([]);
  const [iconsUnfiltered, setIconsUnfiltered] = useState<Icon[]>([]);

  /** Icons loading spinner */
  const [loadingIcons, setLoadingIcons] = useState<boolean>(true);
  // Fetch icons on init
  useEffect(() => {
    axios.get(env.api + `getIcons.php?t=${Date.now()}`).then(response => {
      setLoadingIcons(false);
      const icons = response.data;
      if (icons && Array.isArray(icons)) {
        // Sort
        const sortedIcons: Icon[] = [];
        ICON_NAMES.forEach(iconName => {
          const found = icons.find(icon => icon.name === iconName);
          if (found) {
            sortedIcons.push(found);
          }
        });
        setIcons(sortedIcons);
        setIconsUnfiltered(sortedIcons);
        setFilterKey('');
      }
    }).catch(e => {
      setLoadingIcons(false);
      console.error(e);
    });
  }, []);

  /** Filter icons by ID or translated name */
  const [filterKey, setFilterKey] = useState<string>('');
  useEffect(() => {
    if (filterKey) {
      const normalizedKey = normalize(filterKey).toUpperCase();
      const filtered = new Set<Icon>();
      // Search through specific columns
      iconsUnfiltered.forEach(icon => {
        const keywordsKey = i18n.language === 'en' ? 'keywordsEN' : 'keywordsCS';
        const keywordsMatch = icon[keywordsKey]?.some(keyword =>
          normalize(keyword).toUpperCase().includes(normalizedKey)
        );
        if (icon.name.toUpperCase().includes(normalizedKey) || keywordsMatch) {
          filtered.add(icon);
        }
      });
      setIcons(Array.from(filtered));
    } else {
      setIcons([...iconsUnfiltered]);
    }
  }, [filterKey, iconsUnfiltered, i18n.language]);

  /** Clear filter */
  const filterRef = useRef<HTMLInputElement>(null);
  const clearFilter = () => {
    setFilterKey('');
    filterRef.current?.focus();
  };

  /** All icons downloading status */
  const [downloadingZip, setDownLoadingZip] = useState<boolean>(false);

  /** Download icons in zip file
   * @param icons Icons to include in zip file */
  const downloadZipped = (icons: Icon[]) => {
    if (downloadingZip) { return; }
    setDownLoadingZip(true);
    // Create list of filtered / all icons
    const formData = new FormData();
    if (icons.length) {
      icons.forEach(icon => {
        formData.append('iconNames[]', icon.name);
      });
    } else {
      iconsUnfiltered.forEach(icon => {
        formData.append('iconNames[]', icon.name);
      });
    }
    // Download zip (localhost forces refresh, production OK)
    axios.post(env.api + `getZippedIcons.php?t=${Date.now()}`, formData).then(r => {
      const response = r.data;
      if (response && response.download) {
        const a = document.createElement('a');
        a.setAttribute('href', response.download);
        a.setAttribute('download', 'icons_svg.zip');
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        setTimeout(() => {
          setDownLoadingZip(false);
        }, 1000);
      } else {
        showAlert(t('playground.icons.downloadFailed'));
        setDownLoadingZip(false);
        if (response && response.error) {
          console.error(response.error);
        }
      }
    }).catch(e => {
      setDownLoadingZip(false);
      showAlert(t('playground.icons.downloadFailed'));
      console.error(e);
    });
  };

  /** Icon detail */
  const [isIconModalOpen, setIsIconModalOpen] = useState(false); // Block clicks when modal not open
  const [openIconIndex, setOpenIconIndex] = useState<number>(0);
  const iconModalRef = useRef<HTMLDialogElement | null>(null);

  /** Open icon modal
   * @param iconIndex Index of an icon to show */
  const showIconModal = (iconIndex: number) => {
    setIsIconModalOpen(true);
    setOpenIconIndex(iconIndex);
    iconModalRef.current?.showModal();
    fixBody(true);
  };

  /** Copy icon's XML code */
  const [xmlCopied, setXmlCopied] = useState(false);
  const copiedTimeout = useRef<NodeJS.Timeout | null>(null);
  const copyXml = () => {
    if (!xmlCopied) {
      navigator.clipboard.writeText(icons[openIconIndex].xml);
      setXmlCopied(true);
      copiedTimeout.current = setTimeout(() => {
        setXmlCopied(false);
      }, 2000);
    }
  };

  /** Download individual icon */
  const [downloadingIcon, setDownloadingIcon] = useState(false);
  const downloadIcon = () => {
    setDownloadingIcon(true);
    setTimeout(() => {
      setDownloadingIcon(false);
    }, 1000);
  };

  /** Show next/previous icon in array
   * @param next Show next? */
  const changeIcon = useCallback((next: boolean) => {
    if (copiedTimeout.current) {
      setXmlCopied(false);
      clearTimeout(copiedTimeout.current);
    }
    setOpenIconIndex(prevIndex => (prevIndex + (next ? 1 : -1) + icons.length) % icons.length);
  }, [icons.length]);

  /** Close icon modal */
  const closeIconModal = () => {
    if (copiedTimeout.current) {
      setXmlCopied(false);
      clearTimeout(copiedTimeout.current);
    }
    setIsIconModalOpen(false);
    iconModalRef.current?.close();
    fixBody(false);
  };

  useEffect(() => {
    // Arrows to change icon
    const handleArrows = (e: KeyboardEvent) => {
      if (isIconModalOpen) {
        switch (e.key) {
          case 'ArrowLeft': changeIcon(false); break;
          case 'ArrowRight': changeIcon(true); break;
        }
      }
    };
    window.addEventListener('keyup', handleArrows);

    // Clicking outside the dialog box
    const confirmOutClick = (e: MouseEvent) => {
      if (e.button === 0 && isIconModalOpen && iconModalRef.current) {
        const dialogDimensions = iconModalRef.current.getBoundingClientRect();
        if (e.clientX < dialogDimensions.left ||
          e.clientX > dialogDimensions.right ||
          e.clientY < dialogDimensions.top ||
          e.clientY > dialogDimensions.bottom
        ) {
          closeIconModal();
        }
      }
    };
    window.addEventListener('mousedown', confirmOutClick);

    // Remove listeners
    return () => {
      window.removeEventListener('keydown', handleArrows);
      window.removeEventListener('mousedown', confirmOutClick);
    };
  }, [isIconModalOpen, changeIcon]);

  return (
    <>
      {isCompact && <h3>{t('playground.icons.title')}</h3>}
      <p className="description">{t('playground.icons.description')}</p>

      {!loadingIcons && (!!icons.length || !!filterKey.length) &&
        <div className="toolbar icons-toolbar">
          <span className="input-container">
            <input ref={filterRef} type="text" spellCheck={false} placeholder={t('system.filter')} value={filterKey}
              onInput={(e: React.ChangeEvent<HTMLInputElement>) => setFilterKey(e.target.value)} />
            {!filterKey.length &&
              <div className="icon">
                <Icons.Search aria-hidden="true" />
              </div>
            }
            {!!filterKey.length &&
              <button className="icon button" onClick={clearFilter} tabIndex={-1} aria-label={t('system.clearFilter')}>
                <Icons.CrossSmall aria-hidden="true" />
              </button>
            }
          </span>

          <button className="btn default light download-all-btn"
            onClick={() => downloadZipped(icons)}
            aria-label={t('playground.icons.downloadAllIcons')}
            disabled={downloadingZip}>
            {downloadingZip
              ? <ProgressRing behavior="grow-shrink" />
              : <Icons.Download aria-hidden="true" />}
            {icons.length && filterKey.length
              ? t('playground.icons.downloadFiltered')
              : t('playground.icons.downloadAll')}
          </button>
        </div>
      }

      <div className="icons">
        {loadingIcons &&
          <div className="icons-loading" aria-label={t('system.loading')} role="status">
            <ProgressRing behavior="grow-shrink" />
          </div>
        }

        {!loadingIcons && !icons.length &&
          <div className="error-msg">
            (╯°□°）╯︵ ┻━┻
            {filterKey.length
              ? <p>{t('playground.icons.noMatchingIcons')}</p>
              : <p>{t('playground.icons.iconsFailed')}</p>}
          </div>
        }

        {!loadingIcons && !!icons.length && icons.map((icon, index) =>
          <button
            key={icon.name}
            title={`${t('playground.icons.showDetail')} ${t('iconNames.' + icon.name)}`}
            dangerouslySetInnerHTML={{ __html: icon.xml }}
            onClick={() => showIconModal(index)}
          />
        )}
      </div>

      <dialog ref={iconModalRef} id="icon-modal">
        {!!icons[openIconIndex] && <>

          <div className="header">
            <h3>{t('playground.icons.detail')}</h3>

            <button className="btn default light copy-btn" onClick={copyXml} title={t('playground.icons.copyXML')}>
              {xmlCopied
                ? <AnimatedIcons.Check className="check" aria-hidden="true" />
                : <Icons.Copy aria-hidden="true" />
              }
            </button>

            <a className="btn default light" href={`/assets/icons/${icons[openIconIndex].name}.svg`}
              download={`${icons[openIconIndex].name}.svg`} onClick={downloadIcon} title={t('playground.icons.downloadSVG')}>
              {downloadingIcon
                ? <ProgressRing behavior="grow-shrink" />
                : <Icons.Download aria-hidden="true" />
              }
            </a>

            <button className="btn default light" onClick={closeIconModal} title={t('buttons.close')}>
              <Icons.Cross aria-hidden="true" />
            </button>
          </div>

          <div id="icon" dangerouslySetInnerHTML={{ __html: icons[openIconIndex].xml }} />

          <div className="carousel">
            <button className="btn default light" onClick={() => changeIcon(false)}
              aria-label={t('buttons.previous')} disabled={icons.length <= 1}>
              <Icons.ArrowLeftSmall aria-hidden="true" />
            </button>
            <p>{t('iconNames.' + icons[openIconIndex].name)}</p>
            <button className="btn default light" onClick={() => changeIcon(true)}
              aria-label={t('buttons.next')} disabled={icons.length <= 1}>
              <Icons.ArrowRightSmall aria-hidden="true" />
            </button>
          </div>

        </>}
      </dialog>

    </>
  );
}