import { useEffect, useRef, useState } from 'react';
import axios, { AxiosProgressEvent } from 'axios';
import { useTranslation } from 'react-i18next';
import env from 'env';
// Components
import Checkbox from 'components/ui/checkbox/Checkbox';
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
import ReactSlider from 'react-slider';
// Assets
import * as Icons from 'assets/icons/icons';
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
// Styles
import './InputsPlayground.scss';
import Tooltip from 'components/ui/tooltip/Tooltip';
// Interfaces
interface InputsPlaygroundProps {
  isCompact: boolean;
  showAlert: (alertMsg: string) => void;
}
export default function InputsPlayground(props: InputsPlaygroundProps) {

  /** Props */
  const { isCompact, showAlert } = props;

  /** Translations */
  const { t } = useTranslation();

  /** Main switch state */
  const [allInputsDisabled, setAllInputsDisabled] = useState(false);

  /** Checkboxes */
  const allPlanetsRef = useRef<HTMLInputElement>(null);
  const [planets] = useState(['mercury', 'venus', 'earth', 'mars', 'jupiter', 'saturn', 'uranus', 'neptune']);
  const [checkedPlanets, setCheckedPlanets] = useState<string[]>([]);

  /** Handle individual checkbox change */
  const handleCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const planetName = e.target.name;
    if (e.target.checked) {
      setCheckedPlanets([...checkedPlanets, planetName]);
    } else {
      setCheckedPlanets(checkedPlanets.filter((name) => name !== planetName));
    }
  };

  /** Handle "Check all" checkbox change */
  const handleAllCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const isChecked = e.target.checked;
    setCheckedPlanets(isChecked ? planets : []);
  };

  /** Indeterminate state of the "Check all" checkbox */
  useEffect(() => {
    if (allPlanetsRef.current) {
      allPlanetsRef.current.indeterminate = checkedPlanets.length < planets.length && checkedPlanets.length !== 0;
    }
  }, [checkedPlanets, planets]);

  /** Pluto easter egg */
  const [plutoCounter, setPlutoCounter] = useState(0);
  useEffect(() => {
    if (plutoCounter && !(plutoCounter % 3)) {
      showAlert(t('playground.inputs.plutoAlert'));
    }
  }, [plutoCounter]);

  /** Range slider */
  const [range, setRange] = useState([30, 60]);

  /** File upload */
  const fileInputRef: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
  const [isFileInputHovered, setIsFileInputHovered] = useState(false);
  /** Handle hover & focus on file input */
  useEffect(() => {
    const handleHover = () => {
      setIsFileInputHovered(true);
    };
    const handleUnhover = () => {
      setIsFileInputHovered(false);
    };
    const inputElement = fileInputRef.current;
    // Attach event listeners to the input element
    if (inputElement) {
      inputElement.addEventListener('dragenter', handleHover);
      inputElement.addEventListener('mouseover', handleHover);
      inputElement.addEventListener('focus', handleHover);
      inputElement.addEventListener('drop', handleUnhover);
      inputElement.addEventListener('dragleave', handleUnhover);
      inputElement.addEventListener('mouseleave', handleUnhover);
      inputElement.addEventListener('blur', handleUnhover);
    }
    // Clean up event listeners when component unmounts
    return () => {
      if (inputElement) {
        inputElement.removeEventListener('dragenter', handleHover);
        inputElement.removeEventListener('mouseover', handleHover);
        inputElement.removeEventListener('focus', handleHover);
        inputElement.removeEventListener('drop', handleUnhover);
        inputElement.removeEventListener('dragleave', handleUnhover);
        inputElement.removeEventListener('mouseleave', handleUnhover);
        inputElement.removeEventListener('blur', handleUnhover);
      }
    };
  }, []);

  /** File upload state */
  const [selectedFile, setSelectedFile] = useState<File | null>(null);
  const [fileName, setFileName] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);

  /** Handle change in file input */
  const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedFile(e.target.files ? e.target.files[0] : null);
  };

  /** Clear file input */
  const handleClear = () => {
    setSelectedFile(null);
    // Clear the file input by resetting its value
    if (fileInputRef.current) {
      fileInputRef.current.value = '';
    }
  };

  /** Simulate uploading file */
  useEffect(() => {
    if (selectedFile && !isUploading) {
      setIsUploading(true);
      const formData = new FormData();
      formData.append('file', JSON.stringify(new Array(200000)));
      axios.post(env.api + `simulateUpload.php?t=${Date.now()}`, formData, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        onUploadProgress: (progressEvent: AxiosProgressEvent) => {
          const total = progressEvent.total;
          if (total !== undefined && progressEvent.loaded !== undefined) {
            setUploadProgress((progressEvent.loaded / total) * 100);
          }
        }
      }).then(() => {
        setFileName(selectedFile.name);
      }).catch(() => {
        setIsUploading(false);
        showAlert(t('playground.inputs.uploadFailed'));
      });
    } else {
      setIsUploading(false);
      setUploadProgress(0);
      setFileName('');
    }
  }, [selectedFile]);

  return (
    <>
      {isCompact && <h3>{t('playground.inputs.title')}</h3>}
      <p className="description">{t('playground.inputs.description')}</p>

      <label className="switch">
        <input type="checkbox" onChange={() => setAllInputsDisabled(!allInputsDisabled)} checked={!allInputsDisabled} />
        {allInputsDisabled
          ? t('playground.inputs.inputsDisabled')
          : t('playground.inputs.inputsEnabled')
        }
      </label>

      <label className={`checkbox check-all ${allInputsDisabled ? 'disabled' : ''}`}>
        <input
          ref={allPlanetsRef}
          type="checkbox"
          name="allPlanets"
          checked={checkedPlanets.length === planets.length}
          disabled={allInputsDisabled}
          onChange={handleAllCheckboxChange} />
        <AnimatedIcons.CheckSmall className="check" />
        <AnimatedIcons.Indeterminate className="indeterminate" />
        {t('playground.inputs.allPlanets')}
      </label >
      <div className="checkbox-group">
        {planets.map((planet) => (
          <Checkbox
            key={planet}
            label={t('playground.inputs.' + planet)}
            name={planet}
            checked={checkedPlanets.includes(planet)}
            disabled={allInputsDisabled}
            sendChange={handleCheckboxChange} />
        ))}
        <div onClick={() => setPlutoCounter(prev => prev + 1)} aria-hidden="true">
          <Checkbox
            key="pluto"
            label={t('playground.inputs.pluto')}
            name="pluto"
            checked={false}
            disabled={true}
            sendChange={handleCheckboxChange} />
        </div>
      </div>

      <ReactSlider
        className="range-slider"
        thumbClassName="thumb"
        trackClassName="track"
        min={0}
        max={100}
        value={range}
        onChange={(range: number[]) => setRange(range)}
        ariaLabel={[t('playground.inputs.lowerLimit'), t('playground.inputs.upperLimit')]}
        ariaValuetext={state => t('playground.inputs.limitValue') + state.valueNow}
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        renderThumb={(props: any, state) =>
          <button {...props} disabled={allInputsDisabled}>
            <Tooltip contrast text={state.valueNow} />
          </button>
        }
        pearling
        minDistance={10}
        disabled={allInputsDisabled}
      />

      <span className="input-container file-input">
        <input ref={fileInputRef} type="file"
          title={t('playground.inputs.clickOrDrag')}
          data-translate-title="playground.inputs.clickOrDrag"
          onChange={handleFileChange}
          disabled={allInputsDisabled || isUploading} />

        <div className={`btn default light input ${isFileInputHovered ? 'hover' : ''} ${allInputsDisabled ? 'disabled' : ''}`}>
          {!isUploading && !fileName && <>
            <Icons.Upload aria-hidden="true" />
            {t('playground.inputs.uploadFile')}
          </>}

          {isUploading && <>
            <ProgressRing behavior="progressive" progress={uploadProgress} />
            <label>{fileName || t('playground.inputs.uploading')}</label>
          </>}
        </div>

        {fileName &&
          <button className="icon button" onClick={handleClear}
            aria-label={t('playground.inputs.removeFile')} disabled={allInputsDisabled}>
            <Icons.CrossSmall aria-hidden="true" />
          </button>
        }
      </span >

    </>
  );
}
