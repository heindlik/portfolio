import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
import UmbrellaLoading from 'components/ui/loadings/UmbrellaLoading';
import CogsLoading from 'components/ui/loadings/CogsLoading';
import ThreeDotsLoading from 'components/ui/loadings/ThreeDotsLoading';
import EqualizerLoading from 'components/ui/loadings/EqualizerLoading';
import LinePingLoading from 'components/ui/loadings/LinePingLoading';
import CheckersLoading from 'components/ui/loadings/CheckersLoading';
import PerspectiveLoading from 'components/ui/loadings/PerspectiveLoading';
import GooeyLoading from 'components/ui/loadings/GooeyLoading';

/** Loadings */
export const LOADINGS = [{
  name: 'staticSpinner',
  component: <ProgressRing behavior="static" />,
  html: `
<div class="static-spinner-loading"></div>
  `,
  css: `
.static-spinner-loading {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 3px solid #e7e8eb;
  border-right: 3px solid #0f213b;
  animation: rotate 1s linear infinite;
}

@keyframes rotate {
  100% {
    rotate: 360deg;
  }
}
  `,
  url: 'PodprMd'
}, {
  name: 'growingSpinner',
  component: <ProgressRing behavior="grow-shrink" />,
  html: `
<svg class="growing-spinner-loading" viewBox="0 0 50 50">
  <circle cx="25" cy="25" r="23" />
  <circle class="progress" cx="25" cy="25" r="23" pathLength="1" />
</svg>
  `,
  css: `
.growing-spinner-loading {
  display: block;
  width: 50px;
  height: 50px;
  animation: rotate 2s linear infinite;
}

@keyframes rotate {
  100% {
    rotate: 360deg;
  }
}

.growing-spinner-loading circle {
  fill: none;
  stroke: #e7e8eb;
  stroke-linecap: round;
  stroke-width: 3px;
}

.growing-spinner-loading circle.progress {
  stroke: #0f213b;
  animation: growShrink 2s ease-in-out infinite;
}

@keyframes growShrink {
  0% {
    stroke-dasharray: 0, 1;
    stroke-dashoffset: 0;
  }
  40% {
    stroke-dasharray: 0.5, 1;
    stroke-dashoffset: -0.5;
  }
  100% {
    stroke-dasharray: 0.5, 1;
    stroke-dashoffset: -1;
  }
}
  `,
  url: 'xxadaLd'
}, {
  name: 'spinningUmbrella',
  component: <UmbrellaLoading />,
  html: `
<div class="umbrella-loading"></div>
  `,
  css: `
.umbrella-loading {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 25px solid;
  animation: borderRotation 1s linear infinite;
}

@keyframes borderRotation {
  0,
  100% {
    border-color: rgba(15, 33, 59, 0.8) rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.1) rgba(15, 33, 59, 0.4);
  }
  25% {
    border-color: rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.8) rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.1);
  }
  50% {
    border-color: rgba(15, 33, 59, 0.1) rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.8) rgba(15, 33, 59, 0.4);
  }
  75% {
    border-color: rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.1) rgba(15, 33, 59, 0.4) rgba(15, 33, 59, 0.8);
  }
}
  `,
  url: 'KKxmxyK'
}, {
  name: 'cogs',
  component: <CogsLoading />,
  html: `
<svg class="cogs-loading" viewBox="0 0 50 50">
  <path class="small" d="M27.929,9.109C27.172,9.411 26.462,9.821 25.822,10.325L26.761,12.668C26.214,13.213 25.776,13.856 25.469,14.563L22.944,14.546C22.709,15.326 22.587,16.136 22.582,16.951L24.999,17.678C25.084,18.445 25.314,19.189 25.675,19.87L24.087,21.832C24.551,22.503 25.108,23.104 25.742,23.616L27.818,22.179C28.47,22.591 29.195,22.875 29.953,23.017L30.497,25.482C31.31,25.538 32.128,25.477 32.923,25.3L33.094,22.782C33.823,22.528 34.497,22.139 35.081,21.635L37.347,22.746C37.898,22.145 38.36,21.468 38.718,20.736L36.855,19.032C37.111,18.305 37.227,17.535 37.197,16.764L39.479,15.685C39.353,14.88 39.111,14.097 38.762,13.36L36.268,13.754C35.859,13.1 35.33,12.53 34.708,12.073L35.288,9.616C34.579,9.212 33.816,8.913 33.023,8.727L31.776,10.922C31.01,10.834 30.234,10.892 29.489,11.093L27.929,9.109ZM29.212,13.763C30.999,12.731 33.289,13.345 34.321,15.132C35.353,16.92 34.739,19.209 32.952,20.241C31.164,21.273 28.875,20.66 27.843,18.872C26.811,17.085 27.424,14.795 29.212,13.763Z" />
  <path class="medium" d="M43.243,22.249C42.508,21.97 41.742,21.779 40.962,21.68L39.99,23.605C39.003,23.57 38.019,23.717 37.086,24.039L35.593,22.482C34.876,22.805 34.2,23.212 33.578,23.693L34.252,25.742C33.529,26.414 32.937,27.215 32.505,28.102L30.349,28.057C30.07,28.792 29.879,29.558 29.78,30.338L31.705,31.31C31.67,32.297 31.817,33.281 32.139,34.214L30.582,35.707C30.905,36.424 31.312,37.1 31.793,37.722L33.842,37.048C34.514,37.771 35.315,38.363 36.202,38.795L36.157,40.951C36.892,41.23 37.658,41.421 38.438,41.52L39.41,39.595C40.397,39.63 41.381,39.483 42.314,39.161L43.807,40.718C44.524,40.395 45.2,39.988 45.822,39.507L45.148,37.458C45.871,36.786 46.463,35.985 46.895,35.098L49.051,35.143C49.33,34.408 49.521,33.642 49.62,32.862L47.695,31.89C47.73,30.903 47.583,29.919 47.261,28.986L48.818,27.493C48.495,26.776 48.088,26.1 47.607,25.478L45.558,26.152C44.886,25.429 44.085,24.837 43.198,24.405L43.243,22.249ZM40.982,26.457C43.82,27.165 45.55,30.044 44.843,32.882C44.135,35.72 41.256,37.45 38.418,36.743C35.58,36.035 33.85,33.156 34.557,30.318C35.265,27.48 38.144,25.75 40.982,26.457Z" />
  <path class="large" d="M13.979,10.465C13.145,10.381 12.305,10.381 11.47,10.465L10.984,12.928C10.042,13.092 9.129,13.389 8.27,13.81L6.43,12.103C5.705,12.526 5.025,13.02 4.4,13.578L5.454,15.856C4.789,16.543 4.224,17.32 3.777,18.165L1.284,17.866C0.947,18.633 0.687,19.433 0.509,20.252L2.701,21.476C2.566,22.422 2.566,23.383 2.701,24.329L0.509,25.552C0.687,26.372 0.947,27.171 1.284,27.939L3.777,27.64C4.224,28.485 4.789,29.262 5.454,29.948L4.4,32.227C5.025,32.785 5.705,33.279 6.43,33.701L8.27,31.995C9.129,32.415 10.042,32.712 10.984,32.877L11.47,35.339C12.305,35.423 13.145,35.423 13.979,35.339L14.466,32.877C15.407,32.712 16.321,32.415 17.179,31.995L19.02,33.701C19.745,33.279 20.425,32.785 21.05,32.227L19.996,29.948C20.661,29.262 21.226,28.485 21.673,27.64L24.165,27.939C24.503,27.171 24.763,26.372 24.941,25.552L22.749,24.329C22.884,23.383 22.884,22.422 22.749,21.476L24.941,20.252C24.763,19.433 24.503,18.633 24.165,17.866L21.673,18.165C21.226,17.32 20.661,16.543 19.996,15.856L21.05,13.578C20.425,13.02 19.745,12.526 19.02,12.103L17.179,13.81C16.321,13.389 15.407,13.092 14.466,12.928L13.979,10.465ZM12.725,16.027C16.519,16.027 19.6,19.108 19.6,22.902C19.6,26.697 16.519,29.777 12.725,29.777C8.93,29.777 5.85,26.697 5.85,22.902C5.85,19.108 8.93,16.027 12.725,16.027Z" />
</svg>
  `,
  css: `
.cogs-loading {
  display: block;
  width: 50px;
  height: 50px;
  fill: #0f213b;
}

.cogs-loading .small {
  transform-origin: 31.1px 17px;
  animation: rotate 2s linear infinite reverse;
}

.cogs-loading .medium {
  transform-origin: 39.7px 31.6px;
  animation: rotate calc(2 / 7 * 8s) linear infinite; /* Small cog rotates for 2s and has 7 teeth, medium has 8 */
}

.cogs-loading .large {
  transform-origin: 12.7px 22.9px;
  animation: rotate calc(2 / 7 * 10s) linear infinite; /* Same as above, but large cog has 10 teeth */
}

@keyframes rotate {
  100% {
    transform: rotate(360deg);
  }
}
  `,
  url: 'xxadaQP'
}, {
  name: 'threeDots',
  component: <ThreeDotsLoading />,
  html: `
<div class="three-dots-loading">
  <span></span>
  <span></span>
  <span></span>
</div>
  `,
  css: `
.three-dots-loading {
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  gap: 10px;
}

.three-dots-loading span {
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #0f213b;
  opacity: 0.1;
  scale: 0.8;
  rotate: 0.1deg; /* Bugfix for Firefox scale lag */
  animation: pulseDots 0.8s ease-in-out infinite alternate;
  animation-delay: 0.2s;
}

.three-dots-loading span:first-child {
  animation-delay: 0s;
}

.three-dots-loading span:last-child {
  animation-delay: 0.4s;
}

@keyframes pulseDots {
  0%,
  20% {
    opacity: 0.1;
    scale: 0.8;
  }
  100% {
    opacity: 1;
    scale: 1.1;
  }
}
  `,
  url: 'qBMmJzz'
}, {
  name: 'pulsatingEqualizer',
  component: <EqualizerLoading />,
  html: `
<div class="equalizer-loading">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
  `,
  css: `
.equalizer-loading {
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
}

.equalizer-loading span {
  width: 4px;
  height: 4px;
  border-radius: 2px;
  opacity: 0.1;
  background-color: #0f213b;
  animation: pulseEqualizer 0.8s ease-in-out infinite alternate;
}

.equalizer-loading span:nth-child(2) {
  animation-delay: 0.1s;
}

.equalizer-loading span:nth-child(3) {
  animation-delay: 0.2s;
}

.equalizer-loading span:nth-child(4) {
  animation-delay: 0.3s;
}

.equalizer-loading span:nth-child(5) {
  animation-delay: 0.4s;
}

.equalizer-loading span:nth-child(6) {
  animation-delay: 0.5s;
}

@keyframes pulseEqualizer {
  0%,
  20% {
    height: 4px;
    opacity: 0.1;
  }
  100% {
    height: 20px;
    opacity: 1;
  }
}
  `,
  url: 'XWPRybo'
}, {
  name: 'linePing',
  component: <LinePingLoading />,
  html: `
<div class="line-ping-loading">
  <div class="container">
    <div class="line"></div>
  </div>
</div>
  `,
  css: `
.line-ping-loading {
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
}

.line-ping-loading .container {
  position: relative;
  width: 100%;
  height: 4px;
  border-radius: 2px;
  background-color: #e7e8eb;
}

.line-ping-loading .container .line {
  position: absolute;
  top: 0;
  left: 0;
  right: 40px;
  height: 4px;
  border-radius: 2px;
  background-color: #0f213b;
  animation: linePing 2s ease-in-out infinite;
}

@keyframes linePing {
  0%,
  100% {
    left: 0px;
    right: 40px;
  }
  25% {
    right: 0px;
  }
  50% {
    left: 40px;
    right: 0px;
  }
  75% {
    left: 0px;
  }
}
  `,
  url: 'bGxWQBY'
}, {
  name: 'checkers',
  component: <CheckersLoading />,
  html: `
<div class="checkers-loading">
  <div class="placeholders">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="pieces">
    <span></span>
    <span></span>
    <span></span>
  </div>
</div>
  `,
  css: `
.checkers-loading {
  position: relative;
  width: 50px;
  height: 50px;
}

.checkers-loading .placeholders,
.checkers-loading .pieces {
  position: absolute;
  top: 0;
  left: 0;
  width: 50px;
  height: 50px;
  display: flex;
  flex-wrap: wrap;
}

.checkers-loading span {
  width: 10px;
  height: 10px;
  margin: 7.5px;
  border-radius: 50%;
  background-color: #e7e8eb;
}

.checkers-loading .pieces span {
  background-color: #0f213b;
  animation: checkerMoveDownLeft 1s infinite ease-in-out;
}

.checkers-loading .pieces span:first-child {
  animation: checkerMoveRight 1s infinite ease-in-out;
}

.checkers-loading .pieces span:last-child {
  animation: checkerMoveUp 1s infinite ease-in-out;
}

@keyframes checkerMoveDownLeft {
  0% {
    transform: translate(0, 0);
  }
  25%,
  75% {
    transform: translate(0, 25px);
  }
  100% {
    transform: translate(-25px, 25px);
  }
}

@keyframes checkerMoveRight {
  0%,
  25% {
    transform: translate(0, 0);
  }
  50%,
  100% {
    transform: translate(25px, 0);
  }
}

@keyframes checkerMoveUp {
  0%,
  50% {
    transform: translate(0, 0);
  }
  75%,
  100% {
    transform: translate(0, -25px);
  }
}
  `,
  url: 'BaORGpd'
}, {
  name: 'perspective',
  component: <PerspectiveLoading />,
  html: `
<div class="perspective-loading">
  <span></span>
  <span></span>
</div>
  `,
  css: `
.perspective-loading {
  position: relative;
  width: 50px;
  height: 50px;
}

.perspective-loading span {
  position: absolute;
  left: 0;
  top: 50%;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: #0f213b;
  rotate: 0.1deg; /* Bugfix for Firefox scale lag */
}

.perspective-loading span:first-child {
  animation:
    perspectiveMoveFront 0.6s ease-in-out infinite,
    perspectivePulseFront 0.6s linear infinite;
}

.perspective-loading span:last-child {
  animation:
    perspectiveMoveBack 0.6s ease-in-out infinite,
    perspectivePulseBack 0.6s linear infinite;
}

@keyframes perspectiveMoveFront {
  0% {
    translate: 0 -50%;
  }
  100% {
    translate: 30px -50%;
  }
}

@keyframes perspectivePulseFront {
  0%,
  100% {
    scale: 1;
    opacity: 0.8;
  }
  50% {
    scale: 1.2;
    opacity: 1;
  }
}

@keyframes perspectiveMoveBack {
  0% {
    translate: 30px -50%;
  }
  100% {
    translate: 0 -50%;
  }
}

@keyframes perspectivePulseBack {
  0%,
  100% {
    scale: 1;
    opacity: 0.8;
  }
  50% {
    scale: 0.8;
    opacity: 0.5;
  }
}
  `,
  url: 'VwGbVWa'
}, {
  name: 'gooey',
  component: <GooeyLoading />,
  html: `
<svg class="gooey-loading" filter="url(#gooey)">
  <defs>
    <filter id="gooey" x="0" y="0">
      <feGaussianBlur in="SourceGraphic" stdDeviation="4" result="blur" />
      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="gooey" />
      <feBlend in="SourceGraphic" in2="gooey" />
    </filter>
  </defs>
  <circle class="to" cx="10" cy="25" r="6" />
  <circle class="goo" cx="40" cy="25" r="5" />
  <circle class="from" cx="40" cy="25" r="6" />
</svg>
  `,
  css: `
.gooey-loading {
  display: block;
  margin: 50px auto;
  width: 50px;
  height: 50px;
}

.gooey-loading circle {
  fill: #0f213b;
  rotate: 0.1deg; /* Bugfix for Firefox scale lag */
}

.gooey-loading circle.to {
  transform-origin: left;
  animation: gooGrow 0.5s ease-in-out infinite alternate;
}

.gooey-loading circle.goo {
  transform-origin: center;
  animation: gooMove 1s 0.2s ease-in-out infinite;
}

.gooey-loading circle.from {
  transform-origin: right;
  animation: gooGrow 0.5s ease-in-out infinite alternate-reverse;
}

@keyframes gooGrow {
  0% {
    scale: 1.3;
  }
  100% {
    scale: 0.9;
  }
}

@keyframes gooMove {
  100% {
    translate: -25px 0;
  }
}
  `,
  url: 'MWqmzOL'
}];