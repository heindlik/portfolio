import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { fixBody } from 'utils';
// Code snippets plugin
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { dark } from 'react-syntax-highlighter/dist/esm/styles/prism';
// Components
import PlayPauseIcon from 'components/ui/play-pause-icon/PlayPauseIcon';
import { LOADINGS } from './Loadings';
// Assets
import * as Icons from 'assets/icons/icons';
// Styles
import './LoadingsPlayground.scss';
// Interfaces
interface LoadingsPlaygroundProps {
  isCompact: boolean;
}

export default function LoadingsPlayground(props: LoadingsPlaygroundProps) {

  /** Props */
  const { isCompact } = props;

  /** Translations */
  const { t } = useTranslation();

  /** State of loading animations */
  const [loadingsPlaying, setLoadingsPlaying] = useState<boolean>(true);

  /** Loading detail */
  const [isLoadingModalOpen, setIsLoadingModalOpen] = useState(false); // Block clicks when modal not open
  const loadingModalRef = useRef<HTMLDialogElement | null>(null);
  const [openLoadingIndex, setOpenLoadingIndex] = useState<number>(0);
  const [activeTab, setActiveTab] = useState<'html' | 'css' | 'result'>('result');

  /** Open loading modal
   * @param loadingIndex Index of an loading to show */
  const showLoadingModal = (loadingIndex: number) => {
    setIsLoadingModalOpen(true);
    setOpenLoadingIndex(loadingIndex);
    setActiveTab('result');
    loadingModalRef.current?.showModal();
    fixBody(true);
  };

  /** Show next/previous loading in array
 * @param next Show next? */
  const changeLoading = useCallback((next: boolean) => {
    setOpenLoadingIndex(prevIndex => (prevIndex + (next ? 1 : -1) + LOADINGS.length) % LOADINGS.length);
  }, []);

  /** Scroll code blocks to top on loading change */
  useEffect(() => {
    const codeBlocks = document.querySelectorAll('.prismjs');
    codeBlocks.forEach(block => block.scrollTo(0, 0));
  }, [openLoadingIndex]);

  /** Close loading modal */
  const closeLoadingModal = () => {
    setIsLoadingModalOpen(false);
    loadingModalRef.current?.close();
    fixBody(false);
  };

  /** Keyboard & mouse events */
  useEffect(() => {
    const currentLoadingModalRef = loadingModalRef.current;
    // Arrows to change loading
    const handleArrows = (e: KeyboardEvent) => {
      if (isLoadingModalOpen) {
        switch (e.key) {
          case 'ArrowLeft': changeLoading(false); break;
          case 'ArrowRight': changeLoading(true); break;
        }
      }
    };
    window.addEventListener('keyup', handleArrows);
    // Clicking outside the dialog box
    const confirmOutClick = (e: MouseEvent) => {
      if (e.button === 0 && isLoadingModalOpen && currentLoadingModalRef) {
        const dialogDimensions = currentLoadingModalRef.getBoundingClientRect();
        if (e.clientX < dialogDimensions.left ||
          e.clientX > dialogDimensions.right ||
          e.clientY < dialogDimensions.top ||
          e.clientY > dialogDimensions.bottom
        ) {
          closeLoadingModal();
        }
      }
    };
    window.addEventListener('mousedown', confirmOutClick);
    // Cleanup
    return () => {
      window.removeEventListener('keydown', handleArrows);
      window.removeEventListener('mousedown', confirmOutClick);
    };
  }, [isLoadingModalOpen, changeLoading]);

  return (
    <>
      {isCompact && <h3>{t('playground.loadings.title')}</h3>}
      <p className="description">{t('playground.loadings.description')}</p>

      <div className="toolbar loadings-toolbar">
        <button className="btn default light pause-btn" onClick={() => setLoadingsPlaying(!loadingsPlaying)}>
          <PlayPauseIcon paused={!loadingsPlaying} />
          {loadingsPlaying
            ? t('playground.loadings.pauseAnimations')
            : t('playground.loadings.playAnimations')}
        </button>
      </div>

      <div className={`loadings ${!loadingsPlaying && 'stopped'}`}>
        {LOADINGS.map((loading, index) =>
          <button key={index} className="loading" title={t('playground.loadings.showDetail')}
            onClick={() => showLoadingModal(index)}>
            {loading.component}
          </button>)}
      </div>

      <dialog ref={loadingModalRef} id="loading-modal">
        {!!LOADINGS[openLoadingIndex] && <>

          <div className="header">
            <h3>{t('playground.loadings.detail')}</h3>

            <button className="btn default light" onClick={closeLoadingModal} title={t('buttons.close')}>
              <Icons.Cross aria-hidden="true" />
            </button>
          </div>

          <div className="content">
            <ul className="tablist" role="tablist">
              <li role="presentation">
                <button role="tab" id="html-tab" aria-controls="html-panel"
                  aria-selected={activeTab === 'html'} disabled={activeTab === 'html'} onClick={() => setActiveTab('html')}>
                  <span>html</span>
                </button>
              </li>
              <li role="presentation">
                <button role="tab" id="css-tab" aria-controls="css-panel"
                  aria-selected={activeTab === 'css'} disabled={activeTab === 'css'} onClick={() => setActiveTab('css')}>
                  <span>css</span>
                </button>
              </li>
              <li role="presentation">
                <button role="tab" id="result-tab" aria-controls="result-panel"
                  aria-selected={activeTab === 'result'} disabled={activeTab === 'result'} onClick={() => setActiveTab('result')}>
                  <span>{t('playground.loadings.result')}</span>
                </button>
              </li>
              <li role="presentation">
                <a href={`https://codepen.io/heindlik/pen/${LOADINGS[openLoadingIndex].url}`}
                  target="_blank" rel="noreferrer noopener" aria-label={t('system.openInNewTab', { linkName: 'Codepen' })}>
                  CODEPEN
                  <Icons.ArrowTopRight aria-hidden="true" />
                </a>
              </li>
            </ul>

            <div role="tabpanel" id="html-panel" aria-labelledby="html-tab" hidden={activeTab !== 'html'}>
              <SyntaxHighlighter language={'markup'} style={dark} useInlineStyles={false} wrapLines={true} wrapLongLines={true}>
                {LOADINGS[openLoadingIndex].html}
              </SyntaxHighlighter>
            </div>
            <div role="tabpanel" id="css-panel" aria-labelledby="css-tab" hidden={activeTab !== 'css'}>
              <SyntaxHighlighter language={'css'} style={dark} useInlineStyles={false} wrapLines={true} wrapLongLines={true}>
                {LOADINGS[openLoadingIndex].css}
              </SyntaxHighlighter>
            </div>
            <div role="tabpanel" id="result-panel" aria-labelledby="result-tab" hidden={activeTab !== 'result'}>
              <div id="loading">
                {LOADINGS[openLoadingIndex].component}
              </div>
            </div>
          </div>

          <div className="carousel">
            <button className="btn default light" onClick={() => changeLoading(false)}
              aria-label={t('buttons.previous')} disabled={LOADINGS.length <= 1}>
              <Icons.ArrowLeftSmall aria-hidden="true" />
            </button>
            <p>{t('playground.loadings.' + LOADINGS[openLoadingIndex].name)}</p>
            <button className="btn default light" onClick={() => changeLoading(true)}
              aria-label={t('buttons.next')} disabled={LOADINGS.length <= 1}>
              <Icons.ArrowRightSmall aria-hidden="true" />
            </button>
          </div>

        </>}
      </dialog>
    </>
  );
}