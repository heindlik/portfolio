import { useTranslation } from 'react-i18next';
// Components
import Video from 'components/ui/video/Video';
// Interfaces
interface MediaPlaygroundProps {
  isCompact: boolean;
}
export default function MediaPlayground(props: MediaPlaygroundProps) {

  /** Props */
  const { isCompact } = props;

  /** Translations */
  const { t } = useTranslation();

  return (
    <>
      {isCompact && <h3>{t('playground.media.title')}</h3>}

      <p className="description">{t('playground.media.description')}</p>
      <Video
        title={t('playground.media.shrimpHunting')}
        artist="Tina"
        src="/assets/media/tina.mp4"
      />
    </>
  );
}