import { useTranslation } from 'react-i18next';
import SortIcon from './SortIcon';

// Interface
interface TableHeaderProps {
  field: string;
  sortConfig: { key: string; order: string };
  sortBy: (key: string) => void;
  width?: number; // Fixed width of a column
  minWidth?: number; // Minimum width of a column (used for conditionally last column)
  disabled: boolean;
}

export default function TableHeader(props: TableHeaderProps) {

  /** Props */
  const { field, sortConfig, sortBy, disabled } = props;

  /** Translations */
  const { t } = useTranslation();

  /** Compose aria label based on params
   * @param field Field to sort by
   * @param order Order to sort in */
  const getAriaLabel = (field: string, order: string): string => {
    // Default, initial state (another field is clicked)
    if (sortConfig.key !== field) {
      return `${t('playground.tables.sortBy')} ${t('playground.tables.' + field)} ${t('playground.tables.asc')}`;
    }
    const nextOrder = order === 'init' ? 'asc' : order === 'asc' ? 'desc' : 'init';
    // Next is initial state
    if (nextOrder === 'init') {
      return t('playground.tables.initSort');
    }
    // Next sort by current field in given order
    return `${t('playground.tables.sortBy')} ${t('playground.tables.' + field)} ${t('playground.tables.' + nextOrder)}`;
  };

  return (
    <th style={{ width: props.width, minWidth: props.width || props.minWidth, maxWidth: props.width }}>
      <button type="button" className="btn default" aria-label={getAriaLabel(field, sortConfig.order)}
        onClick={() => sortBy(field)} disabled={disabled}>
        {t('playground.tables.' + field, { defaultValue: field })}
        {!disabled && <SortIcon order={sortConfig.key === field ? sortConfig.order : 'init'} />}
      </button>
    </th>
  );
}