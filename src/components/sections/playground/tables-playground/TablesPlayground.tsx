import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { OCCUPATIONS } from './db/occupationsDB';
import { PEOPLE_FROM_DB } from './db/peopleDB';
import { normalize } from 'utils';
// Components
import TableHeader from './TableHeader';
import Tooltip from 'components/ui/tooltip/Tooltip';
// Assets
import * as Icons from 'assets/icons/icons';
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
// Styles
import './TablesPlayground.scss';
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
// Interfaces
import { ConfirmProps, PromptProps, RawPerson, Person, EmptyRow, Occupation } from 'interfaces';
interface TablesPlaygroundProps {
  isCompact: boolean;
  showAlert: (alertMsg: string) => void;
  showConfirm: (newConfirmProps: Partial<ConfirmProps>) => void;
  showPrompt: (newPromptProps: Partial<PromptProps>) => void;
}

// TODO: Responsive table, split into components?
export default function TablesPlayground(props: TablesPlaygroundProps) {

  /** Props */
  const { isCompact, showConfirm, showPrompt } = props;

  /** Translations */
  const { t } = useTranslation();

  /** Locked edit mode */
  const [isEditable, setIsEditable] = useState(false);
  const toggleEditable = () => {
    // Confirm if there are unsaved changes
    if (changesMade()) {
      showConfirm({
        question: t('playground.tables.confirmDiscardChanges'),
        buttons: [{
          label: t('buttons.storno'),
          class: 'light'
        }, {
          label: t('buttons.continue'),
          action: () => setIsEditable(!isEditable)
        }],
      });
    } else {
      setIsEditable(!isEditable);
    }
  };
  // Hide any forms
  useEffect(() => {
    if (!isEditable) {
      setAddingPerson(false);
      setPeople(prevPeople => prevPeople.map(person => ({ ...person, isEditing: false })));
    }
  }, [isEditable]);

  /** Data */
  const [people, setPeople] = useState<Person[]>([]);
  const [loadingPeople, setLoadingPeople] = useState(true);

  /** Get people from localStorage */
  const getPeople = useCallback(() => {
    const localPeople = localStorage.getItem('tableData');
    if (localPeople) {
      const parsedLocalPeople = JSON.parse(localPeople).map((person: RawPerson) => {
        const foundOccupation = OCCUPATIONS.find((occupation: Occupation) => occupation.id === person.occupationID);
        return {
          ...person,
          occupationID: foundOccupation?.id || 0,
          occupationString: foundOccupation?.title || 'other',
          isEditing: false
        } as Person;
      });
      setPeople(parsedLocalPeople);
      setLoadingPeople(false);
    } else {
      localStorage.setItem('tableData', JSON.stringify(PEOPLE_FROM_DB));
      getPeople();
    }
  }, []);

  /**Fetch people on init */
  useEffect(() => {
    getPeople();
  }, [getPeople]);

  /** Restore table to initial state */
  const restoreTable = () => {
    // Show confirm popup
    showConfirm({
      question: t('playground.tables.confirmRestore'),
      buttons: [{
        label: t('buttons.storno'),
        class: 'light'
      }, {
        label: t('buttons.restore'),
        action: () => {
          setLoadingPeople(true);
          localStorage.removeItem('tableData');
          clearFilter();
          setSortConfig({ key: 'init', order: 'init' });
          setCurrentPageNr(1);
          setTimeout(() => {
            getPeople();
          }, 1000);
        }
      }]
    });
  };

  /** Filtering */
  const filterRef = useRef<HTMLInputElement>(null);
  const [filterKey, setFilterKey] = useState('');
  const clearFilter = () => {
    setFilterKey('');
    filterRef.current?.focus();
  };

  /** Sorting config */
  const [sortConfig, setSortConfig] = useState({ key: 'init', order: 'init' });

  /** Change sorting key & order
   * @param key Field to sort by */
  const sortBy = (key: string) => {
    setSortConfig(prevConfig => {
      let order = 'asc';
      if (prevConfig.key === key) {
        order = prevConfig.order === 'asc'
          ? 'desc'
          : prevConfig.order === 'desc'
            ? 'init'
            : 'asc';
      }
      return { key, order };
    });
  };

  /** Filtered and sorted people */
  const filteredAndSortedPeople: Person[] = useMemo(() => {
    let filteredPeople = [...people];
    // Filter people based on filterKey
    if (filterKey) {
      filteredPeople = filteredPeople.filter((person: Person) =>
        Object.keys(person).some((key) => {
          const value = person[key as keyof Person].toString();
          if (key === 'name' || key === 'surname' || key === 'age') {
            return normalize(value).includes(normalize(filterKey));
          } else if (key === 'occupationString') {
            return normalize(t('occupations.' + value)).includes(normalize(filterKey));
          } else {
            return null;
          }
        })
      );
    }
    // Sort people based on sortConfig
    const { key, order } = sortConfig;
    if (order !== 'init') {
      filteredPeople = [...filteredPeople].sort((a, b) => {
        // Find translation if occupation, else stringify
        const aString = key === 'occupation' ? t('occupations.' + a.occupationString) : a[key as keyof Person]?.toString();
        const bString = key === 'occupation' ? t('occupations.' + b.occupationString) : b[key as keyof Person]?.toString();
        // Numerical sorting if both numbers, string-based if not
        if (!isNaN(+aString) && !isNaN(+bString)) {
          return order === 'asc' ? +aString - +bString : +bString - +aString;
        } else {
          return order === 'asc' ? aString?.localeCompare(bString) || 0 : bString?.localeCompare(aString) || 0;
        }
      });
    }
    // Return the result
    return filteredPeople;
  }, [filterKey, sortConfig, people, t]);

  /** Pagination */
  // const [peoplePerPage, setPeoplePerPage] = useState(5);
  const peoplePerPage = 5;
  const totalPages = Math.ceil(filteredAndSortedPeople.length / peoplePerPage);
  const [currentPageNr, setCurrentPageNr] = useState(1);
  // People on current page
  const currentPagePeople: Person[] = filteredAndSortedPeople.slice(
    (currentPageNr - 1) * peoplePerPage,
    (currentPageNr - 1) * peoplePerPage + peoplePerPage
  );
  // Empty rows to fill the space on last page
  const emptyRows: EmptyRow[] = Array.from(
    { length: peoplePerPage - currentPagePeople.length },
    () => ({ id: '', name: '', surname: '', age: '', occupationID: '', occupationString: '' })
  );
  // Merged people to show on current page
  const currentPage: (Person | EmptyRow)[] = [...currentPagePeople, ...emptyRows];

  /** Form for adding/editing */
  const [form, setForm] = useState<RawPerson>({
    id: 0,
    name: '',
    surname: '',
    age: 0,
    occupationID: 0
  });

  /** Go to specific page when people array length changes */
  const [prevLength, setPrevLength] = useState(0); // Previous length of people array to compare
  const [newPersonID, setNewPersonID] = useState<number | null>(null); // ID of added person to find
  useEffect(() => {
    if (prevLength !== filteredAndSortedPeople.length) {
      if (newPersonID) {
        // Go to page where the new person is
        const indexOfNewPerson = filteredAndSortedPeople.findIndex(person => person.id === newPersonID);
        setCurrentPageNr((Math.ceil(indexOfNewPerson / peoplePerPage)) || 1);
        setNewPersonID(null);
      } else {
        setCurrentPageNr(1);
      }
    }
    setPrevLength(filteredAndSortedPeople.length);
  }, [peoplePerPage, filteredAndSortedPeople, prevLength, newPersonID]);

  /** Show form for adding new person */
  const [addingPerson, setAddingPerson] = useState(false);
  const addPersonNameRef = useRef<HTMLInputElement>(null); // Reference to focus to
  const addPerson = () => {
    // Call to show form
    const showAddingForm = () => {
      setPeople(prevPeople => prevPeople.map(person => ({ ...person, isEditing: false })));
      setForm({
        id: 0,
        name: '',
        surname: '',
        age: 0,
        occupationID: 0
      });
      setAddingPerson(true);
      setTimeout(() => addPersonNameRef.current?.focus(), 0);
    };
    // Confirm if there are unsaved changes
    if (changesMade()) {
      showConfirm({
        question: t('playground.tables.confirmDiscardChanges'),
        buttons: [{
          label: t('buttons.storno'),
          class: 'light'
        }, {
          label: t('buttons.continue'),
          action: showAddingForm
        }],
      });
    } else {
      showAddingForm();
    }
  };

  /** Show editing inputs for clicked row
   * @param personID ID of person to edit */
  const editPerson = (personID: number) => {
    // Call to show form
    const showEditingForm = () => {
      const updatedPeople = people.map(person => {
        if (person.id === personID) {
          const { occupationString: occupationStringOfPerson, isEditing: isEditingPerson, ...rawPerson } = person;
          setForm({ ...rawPerson });
          return { ...person, isEditing: true };
        } else {
          return { ...person, isEditing: false };
        }
      });
      setPeople(updatedPeople);
      setAddingPerson(false);
    };
    // Confirm if there are unsaved changes
    if (changesMade()) {
      showConfirm({
        question: t('playground.tables.confirmDiscardChanges'),
        buttons: [{
          label: t('buttons.storno'),
          class: 'light'
        }, {
          label: t('buttons.continue'),
          action: showEditingForm
        }],
      });
    } else {
      showEditingForm();
    }
  };

  /** Check if changes were made in the form */
  const changesMade = (): boolean => {
    const editedPerson = people.find(person => person.isEditing);
    if (form && editedPerson) { // Changes in people
      // Destructure & compare the data
      const { occupationString: occupationStringOfPerson, isEditing: isEditingPerson, ...editedPersonData } = editedPerson;
      return JSON.stringify(form) !== JSON.stringify(editedPersonData);
    } else if (form && addingPerson) { // Changes in adding-new form
      return form.name !== '' || form.surname !== '' || form.age !== 0 || form.occupationID !== 0;
    } else {
      return false;
    }
  };

  /** Submit new/edited person
   * @param e Submit event */
  const submitPerson = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Confirm if there are unsaved changes
    if (changesMade()) {
      showConfirm({
        question: form.id
          ? t('playground.tables.confirmSaveChanges')
          : t('playground.tables.confirmAddRecord'),
        buttons: [{
          label: t('buttons.storno'),
          class: 'light'
        }, {
          label: form.id
            ? t('buttons.save')
            : t('buttons.add'),
          action: savePerson
        }]
      });
    } else {
      // No changes made, cancel editing
      setPeople(prevPeople => prevPeople.map(person => ({ ...person, isEditing: false })));
    }
  };

  /** Save new/edited person */
  const savePerson = () => {
    setFilterKey('');
    setLoadingPeople(true);
    let updatedPeople: Person[];
    if (form.id) {
      // Update existing person
      updatedPeople = people.map(person => {
        if (person.id === form.id) {
          const foundOccupation = OCCUPATIONS.find((occupation: Occupation) => occupation.id === form.occupationID);
          return {
            ...person,
            ...form,
            occupationID: foundOccupation?.id || 0,
            occupationString: foundOccupation?.title || 'other',
            isEditing: false
          } as Person;
        } else {
          return person;
        }
      });
      setPeople(updatedPeople);
    } else {
      // Add new person
      const highestID = Math.max(...people.map(person => person.id));
      const foundOccupation = OCCUPATIONS.find((occupation: Occupation) => occupation.id === form.occupationID);
      const newPerson: Person = {
        ...form,
        id: highestID + 1,
        occupationID: foundOccupation?.id || 0,
        occupationString: foundOccupation?.title || 'other',
        isEditing: false
      };
      updatedPeople = [...people, newPerson];
      setPeople(updatedPeople);
      // Save new person ID to find its new page after adding
      setNewPersonID(highestID + 1);
    }
    // Save raw people to localStorage
    const rawPeople: RawPerson[] = updatedPeople.map(person => {
      const { occupationString: occupationStringOfPerson, isEditing: isEditingPerson, ...rawPerson } = person;
      return rawPerson;
    });
    localStorage.setItem('tableData', JSON.stringify(rawPeople));
    setAddingPerson(false);
    setTimeout(() => setLoadingPeople(false), 1000);
  };

  /**Remove person from table
   * @param personID ID of person to remove */
  const removePerson = (personID: number) => {
    // Show confirm popup
    showConfirm({
      question: t('playground.tables.confirmRemove'),
      buttons: [{
        label: t('buttons.storno'),
        class: 'light'
      }, {
        label: t('buttons.remove'),
        class: 'danger',
        action: () => {
          setLoadingPeople(true);
          const updatedPeople = people.filter(person => person.id !== personID);
          setPeople(updatedPeople);
          // Save raw people to localStorage
          const rawPeople: RawPerson[] = updatedPeople.map(person => {
            const { occupationString: occupationStringOfPerson, isEditing: isEditingPerson, ...rawPerson } = person;
            return rawPerson;
          });
          localStorage.setItem('tableData', JSON.stringify(rawPeople));
          setAddingPerson(false);
          setTimeout(() => setLoadingPeople(false), 1000);
          // Stay at current page or go to new last if currently at last and it gets removed
          setTimeout(() => {
            const isLastEmpty = updatedPeople.length % peoplePerPage === 0;
            setCurrentPageNr((isLastEmpty && currentPageNr === totalPages) ? currentPageNr - 1 : currentPageNr);
          }, 0);
        }
      }]
    });
  };

  /** Show prompt & export selected data into CSV */
  const exportPeople = () => {
    // Download selected data
    const download = (data: (Person | EmptyRow)[]) => {
      if (data && data.length) {
        setLoadingPeople(true);
        const headers = Object.keys(data[0]).filter(key => key !== 'occupationID' && key !== 'isEditing');
        const csv = [
          headers.map(header => t('playground.tables.' + header)).join(';'),
          ...data.map(person => headers.map(field => {
            if (field === 'occupationString') {
              return t('occupations.' + person[field as keyof Person]);
            } else {
              return JSON.stringify(person[field as keyof Person]);
            }
          }).join(';'))
        ].join('\r\n');
        const csvFile = '\uFEFF' + csv;
        const a = document.createElement('a');
        if (URL && 'download' in a) {
          a.href = URL.createObjectURL(
            new Blob([csvFile], {
              type: 'text/csv;charset=utf-8;encoding:utf-8;'
            })
          );
          a.setAttribute('download', 'data.csv');
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          setTimeout(() => {
            setLoadingPeople(false);
          }, 1000);
        }
      }
    };
    // Prompt for data selection
    const showExportPrompt = () => {
      setPeople(prevPeople => prevPeople.map(person => ({ ...person, isEditing: false })));
      showPrompt({
        prompt: t('playground.tables.confirmExport'),
        type: 'radios',
        options: [
          { label: t('playground.tables.allDataUnsortedUnfiltered'), value: 'allData' },
          { label: t('playground.tables.currentTable'), value: 'currentTable' },
          { label: t('playground.tables.currentPageOnly'), value: 'currentPage' }
        ],
        submitLabel: t('buttons.export'),
        onSubmit: (data: string) => {
          let downloadData;
          switch (data) {
            case 'currentTable': downloadData = filteredAndSortedPeople; break;
            case 'currentPage': downloadData = currentPagePeople; break;
            default: downloadData = people; break;
          }
          download(downloadData);
        }
      });
    };
    // Confirm if there are unsaved changes
    if (changesMade()) {
      showConfirm({
        question: t('playground.tables.confirmDiscardChanges'),
        buttons: [{
          label: t('buttons.storno'),
          class: 'light'
        }, {
          label: t('buttons.continue'),
          action: showExportPrompt
        }],
      });
    } else {
      showExportPrompt();
    }
  };

  return (
    <>
      {isCompact && <h3>{t('playground.tables.title')}</h3>}
      <p className="description">{t('playground.tables.description')}</p>

      <div className="toolbar tables-toolbar">
        <span className="input-container">
          <input ref={filterRef} type="text" spellCheck={false} placeholder={t('system.filter')} value={filterKey}
            onInput={(e: React.ChangeEvent<HTMLInputElement>) => setFilterKey(e.target.value)} />
          {!filterKey.length &&
            <div className="icon">
              <Icons.Search aria-hidden="true" />
            </div>
          }
          {!!filterKey.length &&
            <button className="icon button" onClick={clearFilter} aria-label={t('system.clearFilter')} tabIndex={-1}>
              <Icons.CrossSmall aria-hidden="true" />
            </button>
          }
        </span>

        <span className="count">
          {filteredAndSortedPeople.length}&nbsp;
          {filteredAndSortedPeople.length === 1
            ? t('playground.tables.record')
            : filteredAndSortedPeople.length >= 2 && filteredAndSortedPeople.length <= 4
              ? t('playground.tables.recordsFew')
              : t('playground.tables.recordsMany')
          }
        </span>

        <span className="buttons">
          <button className="btn default light lock-btn" onClick={toggleEditable}
            aria-label={t('playground.tables.' + (isEditable ? 'lockEdits' : 'unlockEdits'))}>
            {isEditable
              ? <Icons.Locked aria-hidden="true" />
              : <Icons.Unlocked aria-hidden="true" />}
            <span>{t('playground.tables.' + (isEditable ? 'lockEdits' : 'unlockEdits'))}</span>
            <Tooltip text={t('playground.tables.' + (isEditable ? 'lockEdits' : 'unlockEdits'))} contrast />
          </button>

          <button className="btn default light restore-btn" onClick={() => restoreTable()} aria-label={t('buttons.restore')}>
            <Icons.Reload aria-hidden="true" />
            <span>{t('buttons.restore')}</span>
            <Tooltip text={t('buttons.restore')} contrast />
          </button>

          <button className="btn default light export-btn" onClick={exportPeople} aria-label={t('buttons.export')}>
            <Icons.Export aria-hidden="true" />
            <span>{t('buttons.export')}</span>
            <Tooltip text={t('buttons.export')} contrast align={window.innerWidth > 600 ? 'right' : undefined} />
          </button>
        </span>
      </div>

      <div className="table-wrapper">
        <form className="horizontal-scroll" onSubmit={submitPerson}>
          <table>
            <thead>
              <tr>
                <TableHeader field="name" sortConfig={sortConfig} sortBy={sortBy} width={150} disabled={loadingPeople} />
                <TableHeader field="surname" sortConfig={sortConfig} sortBy={sortBy} width={150} disabled={loadingPeople} />
                <TableHeader field="age" sortConfig={sortConfig} sortBy={sortBy} width={100} disabled={loadingPeople} />
                <TableHeader field="occupation" sortConfig={sortConfig} sortBy={sortBy} minWidth={210} disabled={loadingPeople} />
                {isEditable && <th style={{ width: 76 }}><button type="button" className="btn default" disabled /></th>}
              </tr>
            </thead>
            <tbody>

              {isEditable && !addingPerson &&
                <tr className="add-row">
                  <td colSpan={5}>
                    <button type="button" onClick={addPerson}>
                      + {t('playground.tables.addRecord')}
                    </button>
                  </td>
                </tr>
              }

              {isEditable && addingPerson &&
                <tr className="editing">
                  <td className="name input">
                    <input ref={addPersonNameRef} type="text" value={form.name} placeholder={t('playground.tables.name')} required
                      onInput={(e: React.ChangeEvent<HTMLInputElement>) => setForm(prev => ({ ...prev, name: e.target.value }))} />
                  </td>
                  <td className="surname input">
                    <input type="text" value={form.surname} placeholder={t('playground.tables.surname')} required
                      onInput={(e: React.ChangeEvent<HTMLInputElement>) => setForm(prev => ({ ...prev, surname: e.target.value }))} />
                  </td>
                  <td className="age input">
                    <input type="text" inputMode="numeric" value={form.age} maxLength={3} required
                      onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const numericValue = e.target.value.replace(/\D|^0+/g, '');
                        setForm((prev) => ({ ...prev, age: +numericValue }));
                      }} />
                  </td>
                  <td className="occupation input">
                    <select value={form.occupationID} onChange={e => setForm(prev => ({ ...prev, occupationID: +e.target.value }))}>
                      <option value={0}>{t('occupations.other')}</option>
                      <option disabled>&#9473;&#9473;&#9473;&#9473;&#9473;</option>
                      {OCCUPATIONS.sort((a, b) => t('occupations.' + a.title).localeCompare(t('occupations.' + b.title))).map(
                        occupation => <option key={occupation.id} value={occupation.id}>{t('occupations.' + occupation.title)}</option>
                      )}
                    </select>
                  </td>
                  <td className="actions">
                    <button type="button" className="btn default light" title={t('buttons.cancel')}
                      onClick={() => setAddingPerson(false)}>
                      <Icons.Back aria-hidden="true" />
                    </button>

                    <button type="submit" className="btn default light" title={t('buttons.add')}>
                      <AnimatedIcons.Check aria-hidden="true" />
                    </button>
                  </td>
                </tr>
              }

              {!!filteredAndSortedPeople.length && currentPage.map((person, index) =>
                <React.Fragment key={index}>
                  {!person.isEditing && (
                    <tr>
                      <td className="name">{person.name ?? '–'}</td>
                      <td className="surname">{person.surname ?? '–'}</td>
                      <td className="age">{person.age ?? '–'}</td>
                      <td className="occupation">
                        {person.occupationString !== ''
                          ? t('occupations.' + person.occupationString)
                          : ''}
                      </td>
                      {isEditable &&
                        <td className="actions">
                          {person.id && <>
                            <button type="button" className="btn default light" title={t('buttons.edit')}
                              onClick={() => editPerson(+person.id)}>
                              <Icons.Pencil aria-hidden="true" />
                            </button>
                            <button type="button" className="btn default light" title={t('buttons.remove')}
                              onClick={() => removePerson(+person.id)}>
                              <Icons.Trash aria-hidden="true" />
                            </button>
                          </>}
                        </td>
                      }
                    </tr>)}
                  {person.isEditing && (
                    <tr className="editing">
                      <td className="name input">
                        <input type="text" value={form.name} placeholder={t('playground.tables.name')} required
                          onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                            setForm(prev => ({ ...prev, name: e.target.value }))} />
                      </td>
                      <td className="surname input">
                        <input type="text" value={form.surname} placeholder={t('playground.tables.surname')} required
                          onInput={(e: React.ChangeEvent<HTMLInputElement>) =>
                            setForm(prev => ({ ...prev, surname: e.target.value }))} />
                      </td>
                      <td className="age input">
                        <input type="text" inputMode="numeric" value={form.age} maxLength={3} required
                          onInput={(e: React.ChangeEvent<HTMLInputElement>) => {
                            const numericValue = e.target.value.replace(/\D|^0+/g, '');
                            setForm((prev) => ({ ...prev, age: +numericValue }));
                          }} />
                      </td>
                      <td className="occupation input">
                        <select value={form.occupationID} onChange={e => setForm(prev => ({ ...prev, occupationID: +e.target.value }))}>
                          <option value={0}>{t('occupations.other')}</option>
                          <option disabled>&#9473;&#9473;&#9473;&#9473;&#9473;</option>
                          {OCCUPATIONS.sort((a, b) => t('occupations.' + a.title).localeCompare(t('occupations.' + b.title))).map(
                            occupation => <option key={occupation.id} value={occupation.id}>{t('occupations.' + occupation.title)}</option>
                          )}
                        </select>
                      </td>
                      {isEditable &&
                        <td className="actions">
                          <button type="button" className="btn default light" title={t('buttons.discardChanges')}
                            onClick={() => setPeople(prevPeople => prevPeople.map(person => ({ ...person, isEditing: false })))}>
                            <Icons.Back aria-hidden="true" />
                          </button>

                          <input type="hidden" value={form.id} />
                          <button type="submit" className="btn default light" title={t('buttons.save')}>
                            <AnimatedIcons.Check aria-hidden="true" />
                          </button>
                        </td>
                      }
                    </tr>
                  )}
                </React.Fragment>
              )}

              {loadingPeople &&
                <tr className="table-loading">
                  <td>
                    <ProgressRing behavior="grow-shrink" />
                  </td>
                </tr>
              }

              {!filteredAndSortedPeople.length &&
                <tr className="error-msg" style={{ height: peoplePerPage * 38 }}>
                  <td colSpan={5}>
                    {!filterKey ? t('playground.tables.noData') : t('playground.tables.noMatch')}
                  </td>
                </tr>
              }
            </tbody>
          </table>
        </form>
      </div>

      <div className="pagination">
        <button className="btn default light" aria-label={t('playground.tables.firstPage')}
          onClick={() => setCurrentPageNr(1)} disabled={currentPageNr <= 1 || loadingPeople}>
          <Icons.First aria-hidden="true" />
        </button>
        <button className="btn default light" aria-label={t('playground.tables.previousPage')}
          onClick={() => setCurrentPageNr(currentPageNr - 1)} disabled={currentPageNr <= 1 || loadingPeople}>
          <Icons.ArrowLeftSmall aria-hidden="true" />
        </button>
        <span aria-label={t('playground.tables.currentPage')}>
          {totalPages !== 0
            ? currentPageNr + ' / ' + totalPages
            : '–'}
        </span>
        <button className="btn default light" aria-label={t('playground.tables.nextPage')}
          onClick={() => setCurrentPageNr(currentPageNr + 1)} disabled={currentPageNr >= totalPages || loadingPeople}>
          <Icons.ArrowRightSmall aria-hidden="true" />
        </button>
        <button className="btn default light" aria-label={t('playground.tables.lastPage')}
          onClick={() => setCurrentPageNr(totalPages)}
          disabled={currentPageNr >= totalPages || loadingPeople}>
          <Icons.Last aria-hidden="true" />
        </button>
      </div>

    </>
  );
}
