import { useTranslation } from 'react-i18next';
import { useParams, useNavigate } from 'react-router-dom';
// Assets
import * as Icons from 'assets/icons/icons';

export default function ProjectDetail() {

  /** Project name (URL param) */
  const { name } = useParams<{ name: string }>();

  /** Router navigation */
  const navigate = useNavigate();

  /** Translations */
  const { t } = useTranslation();

  return (
    <>
      <button className="btn default light" onClick={() => navigate('/projects')}>
        <Icons.ArrowLeft aria-hidden="true" />
        {t('buttons.back')}
      </button>
      <p>{name}</p>
    </>
  );
}