import { useTranslation } from 'react-i18next';
// Assets
import { PROJECTS } from './projectsDB';
import * as Icons from 'assets/icons/icons';
// Styles
import './Projects.scss';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';

export default function ProjectList() {

  /** Translations */
  const { t } = useTranslation();

  return (
    <ul className="project-list">
      {PROJECTS.map((project, index) => (
        <li key={index}>
          <Link to={`/projects/${project.name}`}>
            <h3>
              {t(`projects.${project.name}.title`)}
              {project.private && <Icons.Locked aria-hidden="true" title={t('projects.privateProject')} />}
            </h3>
            <p>{t(`projects.${project.name}.description`)}</p>
            <div className="techs">
              {project.techs.map(tech => (
                <span key={tech}>{tech}</span>
              ))}
            </div>
          </Link>
        </li>
      ))}
      {/* 
  <li>logo nehvizdy</li>
  <li>logo tubis</li> */}
      {/* <div className="links">
  <a className="btn default accent red" href="https://stravenca.heindlik.cz/" target="_blank"
  rel="noreferrer noopener" aria-label={t('system.openInNewTab', { linkName: 'Stravenca' })}>
  <img src="/assets/images/projects/stravenca.png" alt={t('projects.stravencaIcon')} aria-hidden="true" />
  Stravenca
  <Icons.ArrowTopRight aria-hidden="true" />
  </a>
  <a className="btn default accent yellow" href="https://pexeso.heindlik.cz/" target="_blank"
  rel="noreferrer noopener" aria-label={t('system.openInNewTab', { linkName: 'Bačeso' })}>
  <img src="/assets/images/projects/pexeso.png" alt={t('projects.pexesoIcon')} aria-hidden="true" />
  Bačeso
  <Icons.ArrowTopRight aria-hidden="true" />
  </a> */}
    </ul>
  );
}