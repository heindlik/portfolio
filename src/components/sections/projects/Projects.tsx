import { useTranslation } from 'react-i18next';
// Components
import ProjectDetail from './ProjectDetail';
// Assets
import { PROJECTS } from './projectsDB';
import * as Icons from 'assets/icons/icons';
// Styles
import './Projects.scss';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import ProjectList from './ProjectList';

export default function Projects() {

  /** Translations */
  const { t } = useTranslation();

  return (
    <section id="projects">
      <h2>
        <a href="#projects" aria-label={`${t('system.goTo')} ${t('projects.title')}`}>
          {t('projects.title')}
        </a>
      </h2>
      {/* <p className="description">{t('projects.description')}</p> */}

      <Router>
        <Routes>
          <Route path="/projects/:name" Component={ProjectDetail} />
          <Route path="/projects" Component={ProjectList} />
          <Route path="*" Component={ProjectList} />
        </Routes>
      </Router>
    </section >
  );
}