import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
import './Checkbox.scss';
// Interface
type CheckboxProps = {
  label: string;
  name: string;
  checked: boolean;
  disabled: boolean;
  sendChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

export default function Checkbox(props: CheckboxProps) {

  /** Props */
  const { label, name, checked, disabled, sendChange } = props;

  return (
    <label className={`checkbox ${disabled ? 'disabled' : ''}`}>
      <input type="checkbox"
        name={name}
        checked={checked}
        disabled={disabled}
        onChange={sendChange}
      />
      <AnimatedIcons.CheckSmall className="check" aria-hidden="true" />
      <AnimatedIcons.Indeterminate className="indeterminate" aria-hidden="true" />
      {label}
    </label >
  );
}