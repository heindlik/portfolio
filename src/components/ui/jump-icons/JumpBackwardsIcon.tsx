import './JumpIcon.scss';
interface JumpIconProps {
  value: number;
  amount: number;
}

export default function JumpBackwardsIcon(props: JumpIconProps) {

  /** Props */
  const { value, amount } = props;

  return (
    <svg className="jump-icon" viewBox="0 0 18 18" aria-hidden="true">
      <g className="arrow" style={{ rotate: `calc(${amount} * 360deg)` }}>
        <path d="M5.091,7.035L3.091,7.958L2.159,5.958" />
        <path d="M3.091,7.958C3.597,5.091 6.088,3 9,3C12.311,3 15,5.689 15,9C15,12.311 12.311,15 9,15C7.589,15 6.224,14.503 5.143,13.596C6.224,14.503 7.589,15 9,15C12.311,15 15,12.311 15,9C15,5.689 12.311,3 9,3C6.088,3 3.597,5.091 3.091,7.958Z" />
      </g>
      <text x="9" y="9" dy="0.35em">{value}</text>
    </svg>
  );
}