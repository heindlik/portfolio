import './JumpIcon.scss';
interface JumpIconProps {
  value: number;
  amount: number;
}

export default function JumpForwardsIcon(props: JumpIconProps) {

  /** Props */
  const { value, amount } = props;

  return (
    <svg className="jump-icon" viewBox="0 0 18 18" aria-hidden="true">
      <g className="arrow" style={{ rotate: `calc(${amount} * 360deg)` }}>
        <path d="M12.909,7.035L14.909,7.958L15.841,5.958" />
        <path d="M14.909,7.958C14.403,5.091 11.912,3 9,3C5.689,3 3,5.689 3,9C3,12.311 5.689,15 9,15C10.411,15 11.776,14.503 12.857,13.596C11.776,14.503 10.411,15 9,15C5.689,15 3,12.311 3,9C3,5.689 5.689,3 9,3C11.912,3 14.403,5.091 14.909,7.958Z" />
      </g>
      <text x="9" y="9" dy="0.35em">{value}</text>
    </svg>
  );
}