import './Loadings.scss';

export default function CheckersLoading() {
  return (
    <div className="checkers-loading">
      <div className="placeholders">
        <span /><span /><span /><span />
      </div>
      <div className="pieces">
        <span /><span /><span />
      </div>
    </div>
  );
}