import './Loadings.scss';

export default function GooeyLoading() {
  return (
    <svg className="gooey-loading" filter="url(#gooey)">
      <defs>
        <filter id="gooey" x="0" y="0">
          <feGaussianBlur in="SourceGraphic" stdDeviation="4" result="blur" />
          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="gooey" />
          <feBlend in="SourceGraphic" in2="gooey" />
        </filter>
      </defs>
      <circle className="to" cx="10" cy="25" r="6" />
      <circle className="goo" cx="40" cy="25" r="5" />
      <circle className="from" cx="40" cy="25" r="6" />
    </svg>
  );
}