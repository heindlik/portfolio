import './Loadings.scss';

export default function LinePingLoading() {
  return (
    <div className="line-ping-loading">
      <div className="container">
        <div />
      </div>
    </div>
  );
}