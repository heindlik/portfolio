import './Loadings.scss';

export default function PerspectiveLoading() {
  return (
    <div className="perspective-loading">
      <span /><span />
    </div>
  );
}