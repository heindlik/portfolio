import './Loadings.scss';

export default function ThreeDotsLoading() {
  return (
    <div className="three-dots-loading">
      <span /><span /><span />
    </div>
  );
}