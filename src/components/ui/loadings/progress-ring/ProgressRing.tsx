// Styles
import './ProgressRing.scss';
// Interface
interface ProgressProps {
  progress?: number, // percentage
  behavior?: 'static' | 'grow-shrink' | 'progressive'
}

export default function ProgressRing(props: ProgressProps) {

  const progress = props.progress ?? 25;
  const behavior = props.behavior || 'static';

  return (
    <svg className="loading-spinner" viewBox="0 0 18 18" aria-hidden="true">
      <circle cx="9" cy="9" r="8" />
      <circle cx="9" cy="9" r="8" pathLength="1"
        className={`progress ${behavior}`}
        style={{ strokeDashoffset: 1 - (progress / 100) }} />
      {progress >= 100 && <path className="check" d="M5.5,9.5L7.5,11.5L12.5,6.5" pathLength="1" />}
    </svg>
  );
}