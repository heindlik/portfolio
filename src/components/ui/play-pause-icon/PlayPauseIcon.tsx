import { useEffect, useRef } from 'react';
// Interface
interface PlayPauseIconProps {
  paused: boolean;
  noAnimation?: boolean;
}

export default function PlayPauseIcon(props: PlayPauseIconProps) {

  /** Props */
  const { paused, noAnimation } = props;

  /** Animated elements */
  const leftPathAnimation = useRef<SVGAnimateElement>(null);
  const rightPathAnimation = useRef<SVGAnimateElement>(null);

  /** Animate on state change */
  useEffect(() => {
    if (!leftPathAnimation.current || !rightPathAnimation.current) { return; }
    if (paused) {
      leftPathAnimation.current.setAttribute('values', 'M4,3L7,3L7,15L4,15L4,3Z;M4,3L9,6L9,12L4,15L4,3Z');
      rightPathAnimation.current.setAttribute('values', 'M11,3L14,3L14,15L11,15L11,3Z;M9,6L14,9L14,9L9,12L9,6Z');
    } else {
      leftPathAnimation.current.setAttribute('values', 'M4,3L9,6L9,12L4,15L4,3Z;M4,3L7,3L7,15L4,15L4,3Z');
      rightPathAnimation.current.setAttribute('values', 'M9,6L14,9L14,9L9,12L9,6Z;M11,3L14,3L14,15L11,15L11,3Z');
    }
    // Trigger animation
    leftPathAnimation.current.beginElement();
    rightPathAnimation.current.beginElement();
  }, [paused]);

  return (
    <svg viewBox="0 0 18 18" fill="currentColor" aria-hidden="true">
      <path d="M4,3L7,3L7,15L4,15L4,3Z">
        <animate
          ref={leftPathAnimation}
          attributeName="d"
          values="M4,3L7,3L7,15L4,15L4,3Z;M4,3L9,6L9,12L4,15L4,3Z"
          dur={noAnimation ? '0s' : '0.2s'}
          repeatCount="1"
          fill="freeze" />
      </path>
      <path d="M11,3L14,3L14,15L11,15L11,3Z">
        <animate
          ref={rightPathAnimation}
          attributeName="d"
          values="M11,3L14,3L14,15L11,15L11,3Z;M9,6L14,9L14,9L9,12L9,6Z"
          dur={noAnimation ? '0s' : '0.2s'}
          repeatCount="1"
          fill="freeze" />
      </path>
    </svg>
  );
}