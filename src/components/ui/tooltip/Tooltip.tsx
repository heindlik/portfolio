import './Tooltip.scss';
interface TooltipProps {
  text: string | number;
  align?: 'left' | 'right';
  noArrow?: boolean;
  contrast?: boolean; // dark in lightmode, light in darkmode
  colorText?: string; // hardcoded color, ignores contrast
  colorBg?: string; // hardcoded color, ignores contrast
}
export default function Tooltip(props: TooltipProps) {

  /** Props */
  const { text, align, noArrow, contrast, colorText, colorBg } = props;

  /** Tooltip colors */
  const tooltipStyles = {
    '--color-tooltip-text': colorText,
    '--color-tooltip-bg': colorBg,
  } as React.CSSProperties;

  return (
    <div
      className={`tooltip ${align} ${noArrow && 'no-arrow'} ${contrast && 'contrast'}`}
      style={tooltipStyles}>
      {text}
    </div>
  );
}