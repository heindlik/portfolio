import { useCallback, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { formatTime, formatTimeText } from 'utils';
// Components
import Tooltip from 'components/ui/tooltip/Tooltip';
import PlayPauseIcon from 'components/ui/play-pause-icon/PlayPauseIcon';
import JumpBackwardsIcon from 'components/ui/jump-icons/JumpBackwardsIcon';
import JumpForwardsIcon from 'components/ui/jump-icons/JumpForwardsIcon';
import VolumeIcon from 'components/ui/volume-icon/VolumeIcon';
import ProgressRing from 'components/ui/loadings/progress-ring/ProgressRing';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
// Assets
import * as Icons from 'assets/icons/icons';
import * as AnimatedIcons from 'assets/icons/animated/animatedIcons';
// Styles
import './Video.scss';
// Interface
interface VideoProps {
  title: string;
  artist: string;
  album?: string;
  src: string;
}
export default function Video(props: VideoProps) {

  /** Props (video data) */
  const { title, artist, album, src } = props;

  /** Filename for localStorage progress sync */
  const filename = src.split('/').pop();

  /** Translations */
  const { i18n, t } = useTranslation();

  /** Elements */
  const videoContainerRef = useRef<HTMLDivElement>(null);
  const controlsRef = useRef<HTMLDivElement>(null);
  const timelineRef = useRef<HTMLDivElement>(null);
  const videoRef = useRef<HTMLVideoElement>(null);

  /** Loading video data */
  const [isLoading, setIsLoading] = useState(true);

  /** Detect mobile & hide custom GUI */
  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

  /** Error while loading video */
  const [loadingError, setLoadingError] = useState(false);

  /** Mouse activity over the video (to show/hide GUI) */
  const [isMouseActive, setIsMouseActive] = useState(true);
  /** Mouse position over the timeline */
  const [isMouseOverTimeline, setIsMouseOverTimeline] = useState(false);
  /** Mouse position over the controls */
  const [isMouseOverControls, setIsMouseOverControls] = useState(false);
  /** Timeout for hiding UI */
  const uiHidingTimeout = useRef<NodeJS.Timeout | null>(null);

  /** Clicks inside empty space to perform play/pause vs. fullscreen */
  const [emptyClicks, setEmptyClicks] = useState(0);
  /** Timeout for clicks inside empty space */
  const emptyClicksTimeout = useRef<NodeJS.Timeout | null>(null);

  /** Playback status */
  const [isPlaying, setIsPlaying] = useState(false);
  /** Playback status before scrubbing (to resume when scrubbing ends) */
  const wasPlayingBeforeScrub = useRef(false);

  /** Video playback progress percentage in localStorage */
  const storedProgress = +(localStorage.getItem(`videoProgress-${filename}`) || 0);
  /** Playback progress percentage */
  const [playbackProgress, setPlaybackProgress] = useState<number>(!isNaN(storedProgress) ? storedProgress : 0);
  /** Ghost progress on mouse hover */
  const [ghostProgress, setGhostProgress] = useState<number | null>(null);
  /** Timeline scrubbing state */
  const [isTimelineScrubbing, setIsTimelineScrubbing] = useState(false);

  /** Seconds to jump back/forward on btn/arrows */
  const jumpSeconds = 5;
  /** Jump back counter to add icon rotations */
  const [backJumps, setBackJumps] = useState(0);
  /** Jump forward counter to add icon rotations */
  const [forwJumps, setForwJumps] = useState(0);

  /** Volume (from 0 to 1) */
  const [volume, setVolume] = useState<number>(1);
  /** Muted state */
  const [isMuted, setIsMuted] = useState<boolean>(false);
  /** Set initial volume from localStorage */
  useEffect(() => {
    const storedVolume = +(localStorage.getItem('videoVolume') || 1);
    const storedMuted = localStorage.getItem('videoMuted') === 'true';
    setVolume(storedVolume);
    setIsMuted(storedMuted);
    changeVolume(storedVolume, storedMuted);
  }, []);
  /** Memoized volume before it was muted (to restore when unmuted) */
  const volumeBeforeMuted = useRef(volume);
  /** Volume scrubbing state */
  const [isVolumeScrubbing, setIsVolumeScrubbing] = useState(false);


  /** Set initial progress from localStorage once video loads (recursive in case of hiccups) */
  const handleLoadedData = () => {
    if (videoRef.current && videoRef.current.duration && !isNaN(playbackProgress)) {
      videoRef.current.currentTime = playbackProgress === 100 ? 0 : (playbackProgress / 100 * videoRef.current.duration);
      setIsLoading(false);
      setLoadingError(false);
    } else { setTimeout(handleLoadedData, 100); }
  };


  /** Handle error */
  const handleError = () => {
    setIsLoading(false);
    setLoadingError(true);
  };


  /** Show UI on mouse over container */
  const handleMouseInContainer = () => {
    setIsMouseActive(true);
    // Hide UI after 2 seconds of inactivity
    if (uiHidingTimeout.current) { clearTimeout(uiHidingTimeout.current); }
    uiHidingTimeout.current = setTimeout(() => {
      setIsMouseActive(false);
    }, 2000);
  };
  /** Hide UI on mouse leave */
  const handleMouseOutContainer = () => {
    setIsMouseActive(false);
    if (uiHidingTimeout.current) { clearTimeout(uiHidingTimeout.current); }
  };


  /** Register clicks inside empty space */
  const handleEmptyClick = () => {
    if (emptyClicks > 0) {
      toggleFullScreen();
      setEmptyClicks(0);
      if (emptyClicksTimeout.current) { clearTimeout(emptyClicksTimeout.current); }
    } else {
      changeStatusIcon(videoRef.current?.paused ? 'playing' : 'paused');
      setEmptyClicks(prev => prev + 1);
      emptyClicksTimeout.current = setTimeout(() => {
        togglePlayback();
        setEmptyClicks(0);
      }, 300); // double click delay
    }
  };


  /** Controls hover to avoid UI hiding */
  const handleMouseInControls = () => setIsMouseOverControls(true);
  const handleMouseOutControls = () => setIsMouseOverControls(false);


  /** Video focus */
  const [isVideoFocused, setIsVideoFocused] = useState(false);
  const [isFocusedByKeyboard, setIsFocusedByKeyboard] = useState(false);


  /** Timeline */
  const handleTimeUpdate = () => {
    if (!videoRef.current) { return; }
    const progress = (videoRef.current.currentTime / videoRef.current.duration) * 100;
    if (!isNaN(progress)) {
      setPlaybackProgress(progress);
    }
  };
  // Save progress to localStorage TODO: even when video is paused???
  useEffect(() => {
    let previousProgress = 0;
    const savingInterval = setInterval(() => {
      if (!videoRef.current) { return; }
      const progress = (videoRef.current.currentTime / videoRef.current.duration) * 100;
      if (!isNaN(progress) && progress !== previousProgress) {
        localStorage.setItem(`videoProgress-${filename}`, progress.toFixed(4).toString());
        previousProgress = progress;
      }
    }, 1000);
    return () => clearInterval(savingInterval);
  }, [filename]);
  // Handle timeline scrubbing/jumping
  const handleBeforeTimelineScrub = () => {
    // Blur volume slider TODO: mouse out slider?
    const volumeSliderHandle = videoContainerRef.current?.querySelector('.volume .rc-slider-handle') as HTMLDivElement;
    if (volumeSliderHandle) { volumeSliderHandle.blur(); }
    setIsTimelineScrubbing(true);
    wasPlayingBeforeScrub.current = isPlaying;
    togglePlayback(false);
  };
  const handleTimelineScrub = (value: number | number[]) => {
    if (!videoRef.current || typeof value !== 'number') { return; }
    videoRef.current.currentTime = (videoRef.current.duration / 100) * value;
    setPlaybackProgress(value);
  };
  const handleTimelineAfterScrub = () => {
    setIsTimelineScrubbing(false);
    togglePlayback(wasPlayingBeforeScrub.current);
  };
  // Ghost timeline (mouse hover)
  const handleMouseInTimeline = (e: MouseEvent) => {
    if (!timelineRef.current) { return; }
    setIsMouseOverTimeline(true);
    const tRect = timelineRef.current.getBoundingClientRect();
    const width = Math.min(100, Math.max(0, (e.clientX - tRect.left) / tRect.width * 100));
    setGhostProgress(width);
  };
  const handleMouseOutTimeline = () => setIsMouseOverTimeline(false);
  // Handle ghost progress
  useEffect(() => {
    // Remove ghost when not scrubbing & mouse outside timeline
    if (isTimelineScrubbing || (!isTimelineScrubbing && !isMouseOverTimeline)) {
      setGhostProgress(isTimelineScrubbing ? playbackProgress : null);
    }
    // Lose focus after scrub
    const timelineSliderHandle = videoContainerRef.current?.querySelector('.timeline .rc-slider-handle') as HTMLDivElement;
    if (timelineSliderHandle && !isFocusedByKeyboard && !isTimelineScrubbing) { timelineSliderHandle.blur(); }
  }, [playbackProgress, isTimelineScrubbing, isMouseOverTimeline, isFocusedByKeyboard]);
  // Get aria valuetext for elapsed time
  const getElapsedTimeAria = (): string => {
    const video = videoRef.current;
    if (!video) { return ''; }
    return `${formatTimeText(video.currentTime)} ${t('media.of')} ${formatTimeText(video.duration)}`;
  };
  // Jump back in time
  const jumpBackwards = useCallback((offset: number) => {
    if (!videoRef.current) { return; }
    handleMouseInContainer();
    videoRef.current.currentTime -= offset;
    setBackJumps(prev => prev - 1);
  }, []);
  // Jump forward in time
  const jumpForwards = useCallback((offset: number) => {
    if (!videoRef.current) { return; }
    handleMouseInContainer();
    videoRef.current.currentTime += offset;
    setForwJumps(prev => prev + 1);
  }, []);


  /** Status UI */
  const [statusIcon, setStatusIcon] = useState<JSX.Element | null>(null);
  const changeStatusIcon = useCallback((targetStatus: string) => {
    setStatusIcon(null);
    setTimeout(() => {
      switch (targetStatus) {
        case 'playing': setStatusIcon(<PlayPauseIcon paused={false} noAnimation />); break;
        case 'paused': setStatusIcon(<PlayPauseIcon paused={true} noAnimation />); break;
        case 'back': setStatusIcon(<JumpBackwardsIcon value={jumpSeconds} amount={0} />); break;
        case 'forward': setStatusIcon(<JumpForwardsIcon value={jumpSeconds} amount={0} />); break;
        case 'muted': setStatusIcon(<VolumeIcon volume={0} />); showVolume(); break;
        case 'lowVolume': setStatusIcon(<VolumeIcon volume={0.25} />); showVolume(); break;
        case 'highVolume': setStatusIcon(<VolumeIcon volume={1} />); showVolume(); break;
        default: break;
      }
    }, 0);
  }, [jumpSeconds]);
  // Volume UI
  const [isVolumeVisible, setIsVolumeVisible] = useState(false);
  const showVolume = () => {
    setIsVolumeVisible(false);
    setTimeout(() => setIsVolumeVisible(true), 0);
  };


  /** VOLUME */
  /** Change volume explicitly
   * @param volume Volume level (0-1)
   * @param forceMute Mute regardless of volume */
  const changeVolume = (volume: number | number[], forceMute?: boolean) => {
    if (!videoRef.current || typeof volume !== 'number') { return; }
    videoRef.current.volume = volume;
    videoRef.current.muted = forceMute || volume === 0;
  };
  // Mute/unmute through button/key
  const handleToggleMute = useCallback((showStatus?: boolean) => {
    const video = videoRef.current;
    if (!video) { return; }
    video.muted = !video.muted;
    // Restore volume
    video.volume = (video.muted && volumeBeforeMuted.current === 0) ? 0.05 : volumeBeforeMuted.current;
    if (showStatus) { changeStatusIcon(video.muted ? 'muted' : 'highVolume'); }
  }, [changeStatusIcon]);
  // Slider scrubbing
  const handleBeforeVolumeScrub = (volume: number | number[]) => {
    if (typeof volume !== 'number') { return; }
    setIsVolumeScrubbing(true);
    volumeBeforeMuted.current = volume > 0 ? volume : volumeBeforeMuted.current;
  };
  const handleAfterVolumeScrub = (volume: number | number[]) => {
    if (typeof volume !== 'number') { return; }
    setIsVolumeScrubbing(false);
    handleVolumeAfterChange(volume);
  };
  // Detect change (from a miniplayer/arrows)
  const handleVolumeChange = (e: React.ChangeEvent<HTMLVideoElement>) => {
    handleMouseInContainer();
    const { volume, muted } = e.target;
    setVolume(volume);
    setIsMuted(muted);
    // Save to localStorage
    localStorage.setItem('videoMuted', (muted).toString());
  };
  // Save pre-muted volume after slider change 
  const handleVolumeAfterChange = (volume: number | number[]) => {
    if (typeof volume === 'number' && volume > 0) {
      volumeBeforeMuted.current = volume;
      localStorage.setItem('videoVolume', volume.toString());
    }
  };
  // Handle arrow down
  const handleArrowDown = useCallback(() => {
    handleMouseInContainer();
    setVolume(prev => {
      const newVolume = parseFloat((Math.round(Math.min(1, Math.max(0, prev - 0.05)) / 0.05) * 0.05).toFixed(2));
      changeVolume(newVolume, newVolume === 0);
      handleVolumeAfterChange(newVolume);
      changeStatusIcon(newVolume === 0 ? 'muted' : 'lowVolume');
      return newVolume;
    });
  }, [changeStatusIcon]);
  // Handle arrow up
  const handleArrowUp = useCallback(() => {
    handleMouseInContainer();
    setVolume(prev => {
      const newVolume = parseFloat((Math.round(Math.min(1, Math.max(0, prev + 0.05)) / 0.05) * 0.05).toFixed(2));
      changeVolume(newVolume, newVolume === 0);
      handleVolumeAfterChange(newVolume);
      changeStatusIcon(newVolume === 0 ? 'muted' : 'highVolume');
      return newVolume;
    });
  }, [changeStatusIcon]);


  /** Miniplayer */
  const [isMiniPlayerOpen, setIsMiniPlayerOpen] = useState(false);
  const toggleMiniPlayer = useCallback(() => {
    if (!document.pictureInPictureEnabled) { return; }
    if (document.pictureInPictureElement === null) {
      videoRef.current?.requestPictureInPicture();
    } else {
      document.exitPictureInPicture();
    }
  }, []);
  // Detect change
  const handleEnterPiP = () => {
    setIsMiniPlayerOpen(true);
  };
  const handleLeavePiP = useCallback(() => {
    handleMouseInContainer();
    setIsMiniPlayerOpen(false);
  }, []);


  /** Fullscreen */
  const [isFullScreen, setIsFullScreen] = useState(false);
  const toggleFullScreen = useCallback(() => {
    if (!document.fullscreenEnabled) { return; }
    if (document.fullscreenElement === null) {
      videoContainerRef.current?.requestFullscreen();
      // Close PiP
      if (document.pictureInPictureEnabled && document.pictureInPictureElement) { document.exitPictureInPicture(); }
    } else {
      document.exitFullscreen();
    }
  }, []);
  // Detect change
  const handleFullscreenChange = useCallback(() => {
    handleMouseInContainer();
    if (document.fullscreenElement) {
      setIsFullScreen(true);
    } else {
      setIsFullScreen(false);
    }
  }, []);


  /** KEYBOARD SHORTCUTS */
  const handleLastClick = (e: MouseEvent) => {
    const videoContainer = videoContainerRef.current;
    if (e.button === 0 && videoContainer) {
      const videoDimensions = videoContainer.getBoundingClientRect();
      if (e.clientX < videoDimensions.left ||
        e.clientX > videoDimensions.right ||
        e.clientY < videoDimensions.top ||
        e.clientY > videoDimensions.bottom
      ) {
        setIsVideoFocused(false);
      } else {
        setIsVideoFocused(true);
        setIsFocusedByKeyboard(false);
      }
    }
  };
  const handleFocusInContainer = () => {
    setTimeout(() => {
      setIsVideoFocused(true);
      setIsFocusedByKeyboard(true);
      setIsMouseActive(true);
      if (uiHidingTimeout.current) { clearTimeout(uiHidingTimeout.current); }
      uiHidingTimeout.current = setTimeout(() => {
        setIsMouseActive(false);
      }, 2000);
    }, 0);
  };
  // Blur
  const handleBlurOutOfContainer = () => {
    setIsVideoFocused(false);
    setIsMouseActive(false);
    if (uiHidingTimeout.current) { clearTimeout(uiHidingTimeout.current); }
  };


  /** Attach event listeners on init (& react to loading error) */
  useEffect(() => {
    const videoContainer = videoContainerRef.current;
    const controls = controlsRef.current;
    const timelineContainer = timelineRef.current;
    // Assign listeners
    document.addEventListener('click', handleLastClick);
    document.addEventListener('fullscreenchange', handleFullscreenChange);
    document.addEventListener('enterpictureinpicture', handleEnterPiP);
    document.addEventListener('leavepictureinpicture', handleLeavePiP);
    videoContainer?.addEventListener('mousemove', handleMouseInContainer);
    videoContainer?.addEventListener('mouseleave', handleMouseOutContainer);
    controls?.addEventListener('mousemove', handleMouseInControls);
    controls?.addEventListener('mouseleave', handleMouseOutControls);
    timelineContainer?.addEventListener('mousemove', handleMouseInTimeline);
    timelineContainer?.addEventListener('mouseleave', handleMouseOutTimeline);
    // Video controls that can be focused (to keep GUI visible)
    const focusableElements = videoContainer?.querySelectorAll('.rc-slider-handle, button');
    if (focusableElements) {
      focusableElements.forEach(element => {
        element.addEventListener('focus', handleFocusInContainer);
        element.addEventListener('blur', handleBlurOutOfContainer);
      });
    }

    // Cleanup
    return () => {
      document.removeEventListener('click', handleLastClick);
      document.removeEventListener('fullscreenchange', handleFullscreenChange);
      document.removeEventListener('enterpictureinpicture', handleEnterPiP);
      document.removeEventListener('leavepictureinpicture', handleLeavePiP);
      videoContainer?.removeEventListener('mousemove', handleMouseInContainer);
      videoContainer?.removeEventListener('mouseleave', handleMouseOutContainer);
      controls?.removeEventListener('mousemove', handleMouseInControls);
      controls?.removeEventListener('mouseleave', handleMouseOutControls);
      timelineContainer?.removeEventListener('mousemove', handleMouseInTimeline);
      timelineContainer?.removeEventListener('mouseleave', handleMouseOutTimeline);
      // Video controls that can be focused
      const focusableElements = videoContainer?.querySelectorAll('.rc-slider-handle, button');
      if (focusableElements) {
        focusableElements.forEach(element => {
          element.removeEventListener('focus', handleFocusInContainer);
          element.removeEventListener('blur', handleBlurOutOfContainer);
        });
      }
    };
  }, [loadingError, handleFullscreenChange, handleLeavePiP]);


  // TODO: move
  /** GUI state */
  const [isGuiHidden, setIsGuiHidden] = useState(false);
  useEffect(() => {
    setIsGuiHidden((isPlaying && !isMouseActive && !isMouseOverControls && !isMiniPlayerOpen));
  }, [isPlaying, isMouseActive, isMouseOverControls, isMiniPlayerOpen]);


  /** Captions */
  const captionsRef = useRef<HTMLTrackElement>(null);
  const storedShowCaptions = JSON.parse(localStorage.getItem('showCaptions') || 'false');
  const [showCaptions, setShowCaptions] = useState<boolean>(storedShowCaptions);
  const [captionsReady, setCaptionsReady] = useState(false);
  // Show/hide captions
  const toggleCaptions = useCallback(() => {
    const video = videoRef.current;
    if (!video) { return; }
    // Toggle to reflect immediately
    video.textTracks[0].mode = 'hidden';
    video.textTracks[0].mode = showCaptions ? 'showing' : 'hidden';
    localStorage.setItem('showCaptions', showCaptions.toString());
  }, [showCaptions]);
  // Positioning the track above controls
  useEffect(() => {
    const captions = captionsRef.current;
    if (!captions || captions.track.cues === null) { return; }
    // Move track vertically
    Array.from(captions.track.cues).forEach((c) => {
      const line = (c as VTTCue).line === 'auto' ? -2 : +(c as VTTCue).line;
      (c as VTTCue).line = isGuiHidden
        ? line === -4 ? -2 : line === -5 ? -3 : line
        : line === -2 ? -4 : line === -3 ? -5 : line;
    });
    toggleCaptions();
  }, [captionsReady, showCaptions, isGuiHidden, toggleCaptions]);
  // Initialize once loaded
  useEffect(() => {
    const video = videoRef.current;
    video?.addEventListener('loadedmetadata', toggleCaptions);
    return () => video?.removeEventListener('loadedmetadata', toggleCaptions);
  }, [toggleCaptions]);
  // Show button if file exists & is ready TODO: this seems repetitive
  useEffect(() => {
    const captions = captionsRef.current;
    if (!captions) { return; }
    const handleTrackLoad = () => setCaptionsReady(true);
    captions.addEventListener('load', handleTrackLoad);
    return () => captions.removeEventListener('load', handleTrackLoad);
  }, []);


  /** Playback speed */
  const [playbackSpeed, setPlaybackSpeed] = useState(1);
  useEffect(() => {
    const video = videoRef.current;
    if (!video) { return; }
    video.playbackRate = playbackSpeed;
  }, [playbackSpeed]);


  /** Current tooltip (avoids multiple tooltips at the same time) */
  const [focusedTooltip, setFocusedTooltip] = useState<string | null>(null);
  const showTooltip = (target: string) => {
    setFocusedTooltip(currentSettingsPage ? null : target);
  };


  /** Settings pop-up */
  const settingsBtnRef = useRef<HTMLButtonElement>(null);
  /** Settings pop-up */
  const settingsRef = useRef<HTMLDivElement>(null);
  /** Current settings page (which settings page is open) */
  const [currentSettingsPage, setCurrentSettingsPage] = useState<'init' | 'captions' | 'speed' | null>(null);
  /** Delayed change of settings page (content changes faster than click is registered) */
  const delayedSetCurrentSettingsPage = (page: 'init' | 'captions' | 'speed' | null) => {
    setTimeout(() => setCurrentSettingsPage(page), 0);
  };
  /** Open/close settings */
  useEffect(() => {
    const settingsBtn = settingsBtnRef.current;
    const settings = settingsRef.current;
    if (!settingsBtn || !settings) { return; }
    // Handle click
    const handleClick = (e: MouseEvent) => {
      if (settingsBtn.contains(e.target as Node)) {
        setCurrentSettingsPage(prev => {
          setFocusedTooltip(prevTooltip => prev ? prevTooltip : null);
          return prev ? null : 'init';
        });
      } else if (!settings.contains(e.target as Node)) {
        setCurrentSettingsPage(null);
      }
    };
    // Handle Escape
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        setCurrentSettingsPage(null);
      }
    };
    // Settings height
    const setMaxHeight = () => {
      const videoHeight = videoContainerRef?.current?.clientHeight || 0;
      settings.style.maxHeight = (videoHeight - 80) + 'px';
    };
    setMaxHeight();
    // Listeners
    window.addEventListener('click', handleClick);
    window.addEventListener('keydown', handleKeyDown);
    window.addEventListener('resize', setMaxHeight);
    // Cleanup
    return () => {
      window.removeEventListener('click', handleClick);
      window.removeEventListener('keydown', handleKeyDown);
      window.removeEventListener('resize', setMaxHeight);
    };
  }, [i18n.language]);


  /** Play/pause video & show/hide play icon
 * @param play Force state
 * @param showStatus Force status icon display */
  const togglePlayback = useCallback((play?: boolean, showStatus?: boolean) => {
    if (!videoRef.current) { return; }
    if ((videoRef.current.paused && play === undefined) || play) {
      handleMouseInContainer();
      videoRef.current.play();
      setIsPlaying(true);
      if (showStatus) { changeStatusIcon('playing'); }
    } else {
      videoRef.current.pause();
      setIsPlaying(false);
      if (showStatus) { changeStatusIcon('paused'); }
    }
  }, [changeStatusIcon]);
  // Detect play state changes (from a miniplayer or media keys)
  const handlePlaybackChange = () => {
    if (!videoRef.current) { return; }
    setIsPlaying(!videoRef.current.paused);
  };


  /** MediaSession API – e.g. controls on lockscreens */
  useEffect(() => {
    if ('mediaSession' in navigator) {
      // Metadata
      navigator.mediaSession.metadata = new window.MediaMetadata({
        title,
        artist,
        album,
        artwork: [
          { src: '/assets/images/logo-64.jpg', sizes: '64x64', type: 'image/jpeg' },
          { src: '/assets/images/logo-256.jpg', sizes: '256x256', type: 'image/jpeg' },
          { src: '/assets/images/logo-512.jpg', sizes: '512x512', type: 'image/jpeg' }
        ]
      });
      // Action handlers
      const actionHandlers = [
        ['play', () => togglePlayback(true)],
        ['pause', () => togglePlayback(false)],
        ['stop', () => { togglePlayback(false); setPlaybackProgress(0); }],
        ['seekbackward', (details: MediaSessionActionDetails) => jumpBackwards(details.seekOffset || jumpSeconds)],
        ['seekforward', (details: MediaSessionActionDetails) => jumpForwards(details.seekOffset || jumpSeconds)],
        ['seekto', (details: MediaSessionActionDetails) => {
          if (!details.seekTime || !videoRef.current || !videoRef.current.duration) { return; }
          handleTimelineScrub(100 / videoRef.current.duration * details.seekTime);
        }],
      ];
      // Assign action handlers
      for (const [action, handler] of actionHandlers) {
        navigator.mediaSession.setActionHandler(action as MediaSessionAction, handler as MediaSessionActionHandler);
      }
      // Clean up
      return () => {
        navigator.mediaSession.metadata = null;
        for (const [action, handler] of actionHandlers) {
          navigator.mediaSession.setActionHandler(action as MediaSessionAction, handler as MediaSessionActionHandler);
        }
      };
    }
  }, [album, artist, title, jumpBackwards, jumpForwards, togglePlayback]);


  // Shortcuts when focused
  useEffect(() => {
    const handleKeyboard = (e: KeyboardEvent) => {
      const tagName = document.activeElement?.tagName.toLowerCase();
      const isSliderActive = document.activeElement?.classList.contains('rc-slider-handle'); // TODO: remove with rc-slider?
      console.log(tagName, isSliderActive, isVideoFocused);
      if (isVideoFocused && tagName !== 'input' && tagName !== 'textarea' && !isSliderActive) {
        switch (e.key.toLowerCase()) {
          // TODO: this is all sketchy, allow spacebar confirming buttons?
          case ' ': e.preventDefault(); togglePlayback(undefined, true); break;
          case 'k': e.preventDefault(); togglePlayback(undefined, true); break;
          case 'arrowleft': e.preventDefault(); jumpBackwards(jumpSeconds); changeStatusIcon('back'); break;
          case 'arrowright': e.preventDefault(); jumpForwards(jumpSeconds); changeStatusIcon('forward'); break;
          case 'm': e.preventDefault(); handleToggleMute(true); break;
          case 'arrowdown': e.preventDefault(); handleArrowDown(); break;
          case 'arrowup': e.preventDefault(); handleArrowUp(); break;
          case 'i': e.preventDefault(); toggleMiniPlayer(); break;
          case 'f': e.preventDefault(); toggleFullScreen(); break;
        }
      }
    };
    // Assign listener
    document.addEventListener('keydown', handleKeyboard);
    // Cleanup
    return () => {
      document.removeEventListener('keydown', handleKeyboard);
    };
  }, [
    isVideoFocused,
    changeStatusIcon,
    handleArrowDown,
    handleArrowUp,
    handleToggleMute,
    jumpBackwards,
    jumpForwards,
    toggleMiniPlayer,
    toggleFullScreen,
    togglePlayback
  ]);


  return (
    <div ref={videoContainerRef} className="video-container">

      {isLoading && !isPlaying && <ProgressRing behavior="grow-shrink" />}

      {loadingError && !isLoading &&
        <div className="error-msg">
          (╯°□°）╯︵ ┻━┻
          <p>{t('media.videoError')}</p>
        </div>
      }

      {!loadingError && !isMobile &&
        <div className={`controls-container ${isGuiHidden && 'hidden'}`}>

          <button className="empty-space" onClick={handleEmptyClick} tabIndex={-1}>
            {!!statusIcon &&
              <div className="status-icon">{statusIcon}</div>
            }
            {isVolumeVisible &&
              <div className="volume">
                {isMuted ? t('media.muted') : `${t('media.volume')}: ${Math.round(volume * 100)} %`}
              </div>
            }
          </button>

          <div ref={controlsRef} className="controls">
            <div ref={timelineRef} className={`timeline ${(isTimelineScrubbing || focusedTooltip === 'timeline') && 'focused'}`}
              onMouseEnter={() => showTooltip('timeline')} onMouseLeave={() => setFocusedTooltip(null)}>
              <div className="ghost" style={{ width: (ghostProgress || 0) + '%' }}>
                {ghostProgress !== null &&
                  <Tooltip
                    text={formatTime(videoRef.current?.duration ? (ghostProgress * videoRef.current.duration / 100) : 0)}
                    colorText="var(--color-black-ish)"
                    colorBg="var(--color-white-ish)" />
                }
              </div>
              <Slider
                ariaLabelForHandle={t('media.playbackSlider')}
                ariaValueTextFormatterForHandle={getElapsedTimeAria}
                value={playbackProgress}
                onBeforeChange={handleBeforeTimelineScrub}
                onChange={handleTimelineScrub}
                onChangeComplete={handleTimelineAfterScrub}
                onFocus={() => showTooltip('timeline')}
                onBlur={() => setFocusedTooltip(null)}
                keyboard={false}
                step={0.1}
              />
            </div>

            <div className="buttons">
              <button className={`btn default light play-pause-btn ${focusedTooltip === 'play-pause-btn' && 'focused'}`}
                aria-label={`${isPlaying ? t('media.pause') : t('media.play')}: ${t('system.keyboardShortcut')} k`}
                onMouseEnter={() => showTooltip('play-pause-btn')} onFocus={() => showTooltip('play-pause-btn')}
                onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                onClick={() => togglePlayback()}>
                <PlayPauseIcon paused={!isPlaying} />
                <Tooltip
                  noArrow
                  text={(isPlaying ? t('media.pause') : t('media.play')) + ' (k)'}
                  align="left"
                  colorText="var(--color-white-ish)"
                  colorBg="var(--color-black-ish)"
                />
              </button>
              <button className={`btn default light jump-btn ${focusedTooltip === 'jump-back-btn' && 'focused'}`}
                aria-label={`${t('media.jumpBackward')}: ${t('system.keyboardShortcut')} ${t('system.arrowLeft')}`}
                onMouseEnter={() => showTooltip('jump-back-btn')} onFocus={() => showTooltip('jump-back-btn')}
                onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                onClick={() => jumpBackwards(jumpSeconds)}>
                <JumpBackwardsIcon value={jumpSeconds} amount={backJumps} />
                <Tooltip
                  noArrow
                  text={t('media.jumpBackward') + ' (←)'}
                  colorText="var(--color-white-ish)"
                  colorBg="var(--color-black-ish)"
                />
              </button>
              <button className={`btn default light jump-btn ${focusedTooltip === 'jump-forward-btn' && 'focused'}`}
                aria-label={`${t('media.jumpForward')}: ${t('system.keyboardShortcut')} ${t('system.arrowRight')}`}
                onMouseEnter={() => showTooltip('jump-forward-btn')} onFocus={() => showTooltip('jump-forward-btn')}
                onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                onClick={() => jumpForwards(jumpSeconds)}>
                <JumpForwardsIcon value={jumpSeconds} amount={forwJumps} />
                <Tooltip
                  noArrow
                  text={t('media.jumpForward') + ' (→)'}
                  colorText="var(--color-white-ish)"
                  colorBg="var(--color-black-ish)"
                />
              </button>

              <div className={`volume ${isVolumeScrubbing && 'focused'}`}>
                <button className={`btn default light mute-btn ${focusedTooltip === 'mute-btn' && 'focused'}`}
                  aria-label={`${isMuted ? t('media.unmute') : t('media.mute')}: ${t('system.keyboardShortcut')} m`}
                  onMouseEnter={() => showTooltip('mute-btn')} onFocus={() => showTooltip('mute-btn')}
                  onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                  onClick={() => handleToggleMute()}>
                  <VolumeIcon volume={isMuted ? 0 : volume} />
                  <Tooltip
                    noArrow
                    text={(isMuted ? t('media.unmute') : t('media.mute')) + ' (m)'}
                    colorText="var(--color-white-ish)"
                    colorBg="var(--color-black-ish)"
                  />
                </button>

                <div className={`tooltip-container ${focusedTooltip === 'volume-btn' && 'focused'}`}
                  onMouseEnter={() => showTooltip('volume-btn')}
                  onMouseLeave={() => setFocusedTooltip(null)}>
                  <div className="overflow-container">
                    <Slider
                      ariaLabelForHandle={t('media.volumeSlider')}
                      ariaValueTextFormatterForHandle={() => `${Math.round(volume * 100)} %`}
                      value={isMuted ? 0 : volume}
                      onBeforeChange={handleBeforeVolumeScrub}
                      onChange={changeVolume}
                      onChangeComplete={handleAfterVolumeScrub}
                      onFocus={() => showTooltip('volume-btn')}
                      onBlur={() => setFocusedTooltip(null)}
                      max={1}
                      step={0.05}
                    />
                  </div>
                  <Tooltip
                    noArrow
                    text={t('media.volume') + ' (⇅)'}
                    colorText="var(--color-white-ish)"
                    colorBg="var(--color-black-ish)"
                  />
                </div>
              </div>

              <span className="time">
                {formatTime(videoRef.current?.currentTime || 0)}
                {' / '}
                {formatTime(videoRef.current?.duration || 0)}
              </span>

              <span className="end">

                {captionsReady &&
                  <button className={`btn default light captions-btn ${focusedTooltip === 'captions-btn' && 'focused'}`}
                    aria-label={showCaptions ? t('media.hideCaptions') : t('media.showCaptions')}
                    onMouseEnter={() => showTooltip('captions-btn')} onFocus={() => showTooltip('captions-btn')}
                    onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                    onClick={() => setShowCaptions(prevState => !prevState)}>
                    {!showCaptions && <Icons.CaptionsOff aria-hidden="true" />}
                    {showCaptions && <Icons.CaptionsOn aria-hidden="true" />}
                    <Tooltip
                      noArrow
                      text={t('media.captions') + ' (c)'}
                      colorText="var(--color-white-ish)"
                      colorBg="var(--color-black-ish)"
                    />
                  </button>
                }

                <span className="settings-container">
                  <button ref={settingsBtnRef} aria-expanded={!!currentSettingsPage} aria-label={t('media.openSettings')}
                    className={`btn default light settings-btn ${focusedTooltip === 'settings-btn' && 'focused'}`}
                    onMouseEnter={() => showTooltip('settings-btn')} onFocus={() => showTooltip('settings-btn')}
                    onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}>
                    <Icons.Settings aria-hidden="true" />
                    <Tooltip
                      noArrow
                      text={t('media.settings')}
                      colorText="var(--color-white-ish)"
                      colorBg="var(--color-black-ish)"
                    />
                  </button>

                  <div ref={settingsRef} className="settings" aria-hidden={!currentSettingsPage}>
                    {currentSettingsPage === 'init' && <>
                      <button onClick={() => delayedSetCurrentSettingsPage('captions')} disabled>
                        {t('media.captionsSettings')}
                        <Icons.ArrowRightSmall aria-hidden="true" />
                      </button>
                      <button onClick={() => delayedSetCurrentSettingsPage('speed')}>
                        {t('media.playbackSpeed')}
                        <Icons.ArrowRightSmall aria-hidden="true" />
                      </button>
                    </>}

                    {currentSettingsPage === 'captions' && <>
                      <button className="left" onClick={() => delayedSetCurrentSettingsPage('init')} aria-label={t('media.backToSettings')}>
                        <Icons.ArrowLeftSmall aria-hidden="true" />
                        {t('media.captions')}
                      </button>
                    </>}

                    {currentSettingsPage === 'speed' && <>
                      <button className="left" onClick={() => delayedSetCurrentSettingsPage('init')} aria-label={t('media.backToSettings')}>
                        <Icons.ArrowLeftSmall aria-hidden="true" />
                        {t('media.playbackSpeed')}
                      </button>
                      <hr />
                      <div role="radiogroup" aria-label={t('media.playbackSpeed')}>
                        <label className="left">
                          <input type="radio" name="speed" value={2} checked={playbackSpeed === 2}
                            onChange={() => setPlaybackSpeed(2)} aria-label="2x" />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          2x
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={1.75}
                            checked={playbackSpeed === 1.75} onChange={() => setPlaybackSpeed(1.75)}
                            aria-label={(1.75).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US') + 'x'} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {(1.75).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US')}x
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={1.5}
                            checked={playbackSpeed === 1.5} onChange={() => setPlaybackSpeed(1.5)}
                            aria-label={(1.5).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US') + 'x'} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {(1.5).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US')}x
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={1.25}
                            checked={playbackSpeed === 1.25} onChange={() => setPlaybackSpeed(1.25)}
                            aria-label={(1.25).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US') + 'x'} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {(1.25).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US')}x
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={1}
                            checked={playbackSpeed === 1} onChange={() => setPlaybackSpeed(1)}
                            aria-label={t('media.normal')} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {t('media.normal')}
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={0.75}
                            checked={playbackSpeed === 0.75} onChange={() => setPlaybackSpeed(0.75)}
                            aria-label={(0.75).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US') + 'x'} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {(0.75).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US')}x
                        </label>
                        <label className="left">
                          <input type="radio" name="speed" value={0.5}
                            checked={playbackSpeed === 0.5} onChange={() => setPlaybackSpeed(0.5)}
                            aria-label={(0.5).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US') + 'x'} />
                          <AnimatedIcons.CheckSmall aria-hidden="true" />
                          {(0.5).toLocaleString(i18n.language === 'cs' ? 'cs-CZ' : 'en-US')}x
                        </label>
                      </div>
                    </>}
                  </div>
                </span>

                {document.pictureInPictureEnabled &&
                  <button className={`btn default light mini-player-btn ${focusedTooltip === 'mini-player-btn' && 'focused'}`}
                    aria-label={isMiniPlayerOpen ? t('media.closeMiniPlayer') : t('media.openMiniPlayer')}
                    onMouseEnter={() => showTooltip('mini-player-btn')} onFocus={() => showTooltip('mini-player-btn')}
                    onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                    onClick={toggleMiniPlayer}>
                    {!isMiniPlayerOpen && <Icons.MiniPlayer aria-hidden="true" />}
                    {isMiniPlayerOpen && <Icons.MiniPlayerExit aria-hidden="true" />}
                    <Tooltip
                      noArrow
                      text={(isMiniPlayerOpen ? t('media.closeMiniPlayer') : t('media.miniPlayer')) + ' (i)'}
                      colorText="var(--color-white-ish)"
                      colorBg="var(--color-black-ish)"
                    />
                  </button>
                }

                <button className={`btn default light full-screen-btn ${focusedTooltip === 'full-screen-btn' && 'focused'}`}
                  aria-label={isFullScreen ? t('media.exitFullScreen') : t('media.fullScreen')}
                  onMouseEnter={() => showTooltip('full-screen-btn')} onFocus={() => showTooltip('full-screen-btn')}
                  onMouseLeave={() => setFocusedTooltip(null)} onBlur={() => setFocusedTooltip(null)}
                  onClick={toggleFullScreen}>
                  {!isFullScreen && <Icons.FullScreen aria-hidden="true" />}
                  {isFullScreen && <Icons.FullScreenExit aria-hidden="true" />}
                  <Tooltip
                    noArrow
                    text={(isFullScreen ? t('media.exitFullScreen') : t('media.fullScreen')) + ' (f)'}
                    align="right"
                    colorText="var(--color-white-ish)"
                    colorBg="var(--color-black-ish)"
                  />
                </button>
              </span>
            </div>
          </div>
        </div>
      }

      <video ref={videoRef} src={src} controls={isMobile}
        onLoadedData={handleLoadedData}
        onError={handleError}
        onPlay={handlePlaybackChange}
        onPause={handlePlaybackChange}
        onEnded={handlePlaybackChange}
        onTimeUpdate={handleTimeUpdate}
        onVolumeChange={handleVolumeChange}
        onSeeking={() => setIsLoading(true)}
        onSeeked={() => setIsLoading(false)}
        autoPlay={isMobile} // TODO: Test on android
      >
        <track
          ref={captionsRef}
          kind="captions"
          label={t('media.langCaptions')}
          srcLang={i18n.language}
          src={`/assets/media/${filename?.split('.')[0]}_${i18n.language}.vtt`}
          default
        />
      </video>
    </div>
  );
}