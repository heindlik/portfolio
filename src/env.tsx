/** Requires Apache running locally */
const devConfig = {
  api: 'http://portfolio-backend.localhost/',
};

/** Requires Apache running on production server */
const prodConfig = {
  api: '/backend/',
};

export default process.env.NODE_ENV === 'production' ? prodConfig : devConfig;