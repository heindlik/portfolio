import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles.scss';
import { I18nextProvider } from 'react-i18next';
import i18n from './locales/i18n';
import App from './components/App';

/** ASAP color mode switch to avoid FOUC */
const colorMode = localStorage.getItem('mode') || 'auto';
const systemDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
document.documentElement.classList.add(colorMode === 'auto' && systemDark ? 'dark' : colorMode);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <I18nextProvider i18n={i18n}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </I18nextProvider>
);
