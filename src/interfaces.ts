/** Alert properties */
export interface AlertInterface {
  message: string;
  timestamp: number;
  type?: 'success' | 'warning' | 'error';
}

/** Confirm properties */
export interface ConfirmProps {
  isOpen: boolean;
  question: string;
  buttons: Button[];
  canEscape?: boolean;
  onClose: () => void;
}

/** Button properties */
export interface Button {
  label: string;
  class?: 'light' | 'danger';
  action?: () => void;
}

/** Prompt properties */
export interface PromptProps {
  isOpen: boolean;
  prompt: string;
  type: 'text' | 'number' | 'radios' | 'select';
  options?: { value: string, label: string }[]; // radios/options to map through
  submitLabel: string;
  onSubmit: (value: string) => void;
  canEscape?: boolean;
  onClose: () => void;
}

/** Project */
export interface Project {
  name: string;
  private: boolean;
  url: string;
  gitUrl: string;
  techs: string[];
  images: ProjectImage[];
}

/** Project image */
export interface ProjectImage {
  filename: string;
  caption: string;
  alt: string;
}

/** Occupations for select */
export interface Occupation {
  id: number;
  title: string;
}

/** Raw fetched table data */
export interface RawPerson {
  id: number;
  name: string;
  surname: string;
  age: number;
  occupationID: number;
}

/** Table data */
export interface Person extends RawPerson {
  occupationString: string;
  isEditing: boolean;
}

/** Empty table row (contains hyphens) */
export interface EmptyRow {
  [key: string]: string
}