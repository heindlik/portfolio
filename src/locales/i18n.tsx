import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';
import translationCS from './cs.json';
import translationEN from './en.json';
import iconTranslationsCS from './icons/cs.json';
import iconTranslationsEN from './icons/en.json';
import occupationsCS from './occupations/cs.json';
import occupationsEN from './occupations/en.json';

i18n.use(Backend).use(initReactI18next).init({
  resources: {
    cs: {
      translation: {
        ...translationCS,
        ...iconTranslationsCS,
        ...occupationsCS
      }
    },
    en: {
      translation: {
        ...translationEN,
        ...iconTranslationsEN,
        ...occupationsEN
      }
    }
  },
  lng: 'cs',
  fallbackLng: 'cs',
  debug: true,
  interpolation: {
    escapeValue: false
  },
});

export default i18n;