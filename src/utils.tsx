/** Offset to fix at & return to */
let offsetYBeforeFix = 0;

/** Status for precision */
let isBodyFixed = false;

/** Fix/unfix body to avoid scrolling
 * @param fix Fix / unfix */
export const fixBody = (fix: boolean) => {
  // TODO: unfix only when not loading, HOW?
  if (fix && !isBodyFixed) {
    offsetYBeforeFix = window.scrollY;
    document.body.style.position = 'fixed';
    document.body.style.top = -offsetYBeforeFix + 'px';
    isBodyFixed = true;
  } else if (isBodyFixed) {
    document.body.style.position = 'unset';
    document.documentElement.style.scrollBehavior = 'unset';
    window.scrollTo(0, offsetYBeforeFix);
    document.documentElement.style.scrollBehavior = 'smooth';
    isBodyFixed = false;
  }
};

/** Scroll to top & remove hash */
export const scrollToTop = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
  if (window.location.hash) {
    window.history.pushState('', document.title, window.location.pathname + window.location.search);
  }
};

/** Convert input to comparable lowercase string
 * @param string Value to convert */
export const normalize = (string: string): string => {
  let loweredString = string.toString().toLowerCase();
  const replaceIt = 'áäčďéěíĺľňóôöŕšťúůüýřž';
  const withThese = 'aacdeeillnooorstuuuyrz';
  for (let i = 0; i < replaceIt.length; i++) {
    while (loweredString.indexOf(replaceIt[i]) !== -1) {
      loweredString = loweredString.replace(replaceIt[i], withThese[i]);
    }
  }
  return loweredString;
};

/** Formats seconds into a readable string (HH:MM:SS)
 * @param time Time in seconds */
export const formatTime = (time: number): string => {
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor((time % 3600) / 60);
  const seconds = Math.floor(time % 60);
  // Format
  const formattedHours = hours > 0 ? hours.toString() : '';
  const formattedMinutes = (hours > 0) ? minutes.toString().padStart(2, '0') : minutes.toString();
  const formattedSeconds = seconds.toString().padStart(2, '0');
  // Return
  return `${formattedHours}${formattedHours && formattedMinutes ? ':' : ''}${formattedMinutes}:${formattedSeconds}`;
};

/** Formats seconds into an aria text
 * @param time Time in seconds */
export const formatTimeText = (time: number): string => {
  const hours = Math.floor(time / 3600);
  const minutes = Math.floor((time % 3600) / 60);
  const seconds = Math.floor(time % 60);
  // Format
  const formattedHours = hours > 0 ? (hours + ' h ') : '';
  const formattedMinutes = (hours > 0 || minutes > 0) ? (minutes + ' min ') : '';
  // Return
  return formattedHours + formattedMinutes + seconds + ' s';
};

// TODO: this is now redundant... (alerts no longer stored)
/** Formats time in the past to readable string relative to now
 * @param timestamp Time in milliseconds
 * @param lang Language to translate to */
export const formatPastTimestamp = (timestamp: number, lang: string): string => {
  const locale = lang === 'cs' ? 'cs-CZ' : 'en-US';
  const date = new Date(timestamp);
  const now = Date.now();
  // Return plain if future
  if (date > new Date()) { return date.toLocaleString(); }
  // Local time
  const localTime = date.toLocaleString(locale, { hour: 'numeric', minute: '2-digit' });
  // Elapsed time
  const elapsedMs = now - timestamp;
  const elapsedDays = Math.floor((elapsedMs / 86400000));
  const elapsedHours = Math.floor((elapsedMs % 86400000) / 3600000);
  const elapsedMinutes = Math.floor((elapsedMs % 3600000) / 60000);
  // Conditions
  const wasToday = date.toDateString() === new Date(now).toDateString();
  const wasYesterday = date.toDateString() === new Date(now - 86400000).toDateString();
  // Result
  if (wasToday) {
    if (elapsedHours > 3) {
      return localTime;
    } else if (elapsedHours) {
      return lang === 'cs'
        ? `před ${elapsedHours} hod`
        : `${elapsedHours}h ago`;
    } else if (elapsedMinutes) {
      return lang === 'cs'
        ? `před ${elapsedMinutes} min`
        : `${elapsedMinutes}m ago`;
    } else {
      return lang === 'cs'
        ? 'teď'
        : 'now';
    }
  } else if (wasYesterday) {
    return lang === 'cs'
      ? `Včera ${localTime}`
      : `Yesterday, ${localTime}`;
  } else if (elapsedDays < 7 && date.getDay() !== new Date().getDay()) {
    return date.toLocaleString(locale, { weekday: 'short', hour: 'numeric', minute: '2-digit' });
  } else {
    return date.toLocaleString(locale, { day: 'numeric', month: 'short', year: 'numeric' });
  }
};

/** Prevent form submission on Enter
 * @param e Keyboard event */
export const blockEnter = (e: React.KeyboardEvent<HTMLInputElement>) => {
  if (e.key === 'Enter') {
    e.preventDefault();
  }
};
